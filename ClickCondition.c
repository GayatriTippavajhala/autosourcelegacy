ClickCondition()
{

lr_start_transaction("RMS_AutoSource_T11_ClickCondition");

//	lr_start_transaction("AS_Service_VidSvr_GetOptions");

	lr_start_sub_transaction("RMS_AutoSource_T11_ClickCondition_ST00_Service_VidSvr_GetOptions", "RMS_AutoSource_T11_ClickCondition");
		
		web_reg_save_param_ex("ParamName=VID_captured1", "LB=\"Response\":{\"@VID\":\"",	"RB=\"",
			SEARCH_FILTERS,
			"Scope=Body",
			"IgnoreRedirections=No",
			LAST);

		web_custom_request("VidSvr_5",
			"URL={ADXE_URL}/api/Service/VidSvr", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Vehicle", 
			"Snapshot=t56.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"Request\":{\"@Method\":\"GetOptions\",\"@type\":\"undefined\",\"Engine\":\"0\",\"Make\":5,\"Model\":0,\"Region\":\"Asian\",\"Style\":7,\"Transmission\":\"0\",\"Year\":4}}}", 
			LAST);

	lr_end_sub_transaction("RMS_AutoSource_T11_ClickCondition_ST00_Service_VidSvr_GetOptions", LR_AUTO);

	
//	lr_end_transaction("AS_Service_VidSvr_GetOptions", LR_AUTO);
	
//	lr_start_transaction("AS_Service_VidSvr_SetOptions");
	
	lr_start_sub_transaction("RMS_AutoSource_T11_ClickCondition_ST01_Service_VidSvr_SetOptions", "RMS_AutoSource_T11_ClickCondition");
	
				web_reg_save_param_ex("ParamName=VID_captured2", "LB=\"Response\":{\"@VID\":\"",	"RB=\"",
					SEARCH_FILTERS,
					"Scope=Body",
					"IgnoreRedirections=No",
					LAST);
	
				web_reg_save_param_ex("ParamName=VID_captured3", "LB=\"Response\":{\"@VID\":\"",	"RB=                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ",
					SEARCH_FILTERS,
					"Scope=Body",
					"IgnoreRedirections=No",
					LAST);
			web_custom_request("VidSvr_5", 
				"URL={ADXE_URL}/api/Service/VidSvr", 
				"Method=POST", 
				"Resource=0", 
				"RecContentType=text/plain", 
				"Referer={ADXE_URL}/Estimating/Autosource/Vehicle", 
				"Snapshot=t57.inf", 
				"Mode=HTML", 
				"EncType=application/json;charset=UTF-8", 
				"Body={\"Form\":{\"Request\":{\"@Method\":\"SetOptions\",\"@type\":\"undefined\",\"ExteriorOpt\":0,\"InteriorOpt\":-1,\"Item\":[\"AAI\",\"ABS\",\"AC \",\"ADM\",\"AHC\",\"ALR\",\"ALW\"," 
				"\"AMP\",\"BST\",\"CC \",\"CD \",\"CHG\",\"CPS\",\"CTC\",\"DAB\",\"DEF\",\"DHM\",\"DSM\",\"DZA\",\"ES2\",\"EST\",\"FCA\",\"FLM\",\"FOG\",\"GDO\",\"HAB\",\"HAH\",\"HFS\",\"INW\"," 
				"\"IVM\",\"KES\",\"LCD\",\"LDA\",\"LED\",\"LES\",\"LRL\",\"LSW\",\"LTH\",\"MP3\",\"OHC\",\"PB \",\"PDL\",\"PL \",\"PMO\",\"PW \",\"RBS\",\"RHA\",\"RTR\",\"RVC\",\"SAB\",\"SCS\"," 
				"\"SVC\",\"SWC\",\"SXS\",\"TCH\",\"TCM\",\"TCS\",\"TNT\",\"TPM\",\"TTW\",\"WPC\",\"WST\",\"\"],"
				"\"UOpt\":[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"CZ\",\"\",\"FC\",\"\",\"\",\"\",\"HX\",\"\",\"\",\"\",\"\",\"2I\",\"9R\",\"\",\"" 
				"DY\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"MR\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"JP\"],\""
				"VID\":\"{VID_captured1}\"}}}",
				LAST);
//	lr_end_transaction("AS_Service_VidSvr_SetOptions", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T11_ClickCondition_ST01_Service_VidSvr_SetOptions", LR_AUTO);

	lr_save_string("AAI|Auxiliary Audio Input|S|0|||^ABS|Anti-Lock Brakes|S|1|||^AC |Air Conditioning|S|2|||^ADM|Automatic Dimming Mirror|S|3|||^AHC|Auto Headlamp Control|S|4|||^ALR|Alarm System|S|5|||^ALW|Aluminum/Alloy Wheels|S|6|||^AMP|Amplifier|S|7|||^BST|Bucket Seats|S|8|||^CC |Cruise Control|S|9|||^CD |AM/FM CD Player|S|10|||^CHG|Chrome Grille|S|11|||^CPS|Compact Spare Tire|S|12|||^CTC|Center Console|S|13|||^DAB|Dual Airbags|S|14|||^DEF|Rear Window Defroster|S|15|||^DHM|Heated Power Mirrors|S|16|||^DSM|Driver Seat Memory|S|17|||^DZA|Dual Zone Auto A/C|S|18|||^ES2|Dual Power Seats|S|19|||^EST|Electric Steering|S|20|||^FCA|Fwd. Collision Alert|S|21|||CZ^FLM|Floor Mats|S|22|||^FOG|Fog Lights|S|23|||FC^GDO|Garage Door Opener|S|24|||^HAB|Head Airbags|S|25|||^HAH|Halogen Headlights|S|26|||^HFS|Heated Front Seats|S|27|||HX^INW|Intermittent Wipers|S|28|||^IVM|Illuminated Visor Mirror|S|29|||^KES|Keyless Entry System|S|30|||^LCD|1st Row LCD Monitor(s)|S|31|||^LDA|Lane Departure Alert|S|32|||2I^LED|LED Brakelights|S|33|||9R^LES|Lighted Entry System|S|34|||^LRL|LED Daytime Running Lts|S|35|||DY^LSW|Leather Steering Wheel|S|36|||^LTH|Leather Seats|S|37|||^MP3|MP3 Decoder|S|38|||^OHC|Overhead Console|S|40|||^PB |Power Brakes|S|41|||^PDL|Pwr Driver Lumbar Supp|S|42|||^PL |Power Door Locks|S|43|||^PMO|Power Moonroof|S|44|||MR^PW |Power Windows|S|45|||^RBS|Rear Bench Seat|S|46|||^RHA|2nd Row Head Airbags|S|47|||^RTR|Rem Trunk-L/Gate Release|S|49|||^RVC|Rear View Camera|S|50|||^SAB|Side Airbags|S|51|||^SCS|Stability Cntrl Suspensn|S|52|||^SVC|Side View Camera(s)|S|53|||^SWC|Strg Wheel Radio Control|S|54|||^SXS|SiriusXM Satellite Radio|S|55|||^TCH|Tachometer|S|56|||^TCM|Trip Computer|S|57|||^TCS|Traction Control System|S|58|||^TNT|Tinted Glass|S|59|||^TPM|Tire Pressure Monitor|S|60|||^TTW|Tilt & Telescopic Steer|S|61|||^WPC|Wireless Phone Connect|S|62|||^WST|Wireless Audio Streaming|S|63|||","HID_Options1");
	lr_save_string("AAI^ABS^AC ^ADM^AHC^ALR^ALW^AMP^BST^CC ^CD ^CHG^CPS^CTC^DAB^DEF^DHM^DSM^DZA^ES2^EST^FCA^FLM^FOG^GDO^HAB^HAH^HFS^INW^IVM^KES^LCD^LDA^LED^LES^LRL^LSW^LTH^MP3^OHC^PB ^PDL^PL ^PMO^PW ^RBS^RHA^RTR^RVC^SAB^SCS^SVC^SWC^SXS^TCH^TCM^TCS^TNT^TPM^TTW^WPC^WST^JP","HID_Sel_OPT_Value");
//	lr_start_transaction("AS_Service_ClaimUpdate_4");
	
	lr_start_sub_transaction("RMS_AutoSource_T11_ClickCondition_ST02_Service_ClaimUpdate", "RMS_AutoSource_T11_ClickCondition");
	
		web_custom_request("ClaimUpdate_4", 
			"URL={ADXE_URL}/api/Service/ClaimUpdate", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/PointOfImpact", 
			"Snapshot=t68.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"HidSelPack\":{\"fieldtype\":\"hiddendata\",\"name\":\"HidSelPack\",\"caption\":\"{HidSelPack}\",\"value\":null},\"HidSelOpt\":{\"fieldtype\":\"hiddendata\",\"name\":\"HidSelOpt\",\"caption\":\"{HidSelOpt}\",\"value\":\"{HID_Sel_OPT_Value}\"},\"HidSelAft\":{\"fieldtype\":\"hiddendata\",\"name\":\"HidSelAft\",\"caption\":\"{HidSelAft}\""
			",\"value\":null},\"Layer\":{\"fieldtype\":\"hiddendata\",\"name\":\"Layer\",\"caption\":\"{Layer}\",\"value\":null},\"ICBCmode\":{\"fieldtype\":\"hiddendata\",\"name\":\"ICBCmode\",\"caption\":\"{ICBCmode}\",\"value\":\"N\"},\"Travelersmode\":{\"fieldtype\":\"hiddendata\",\"name\":\"Travelersmode\",\"caption\":\"{Travelersmode}\",\"value\":\"N\"},\"AudaVINEnabled\":{\"fieldtype\":\"hiddendata\",\"name\":\"AudaVINEnabled\",\"caption\":\"{AudaVINEnabled}\",\"value\":\"0\"},\"AudaVINXml\":{\""
			"fieldtype\":\"hiddendata\",\"name\":\"AudaVINXml\",\"caption\":\"{AudaVINXml}\",\"value\":null},\"AudaVINCalledFlag\":{\"fieldtype\":\"hiddendata\",\"name\":\"AudaVINCalledFlag\",\"caption\":\"{AudaVINCalledFlag}\",\"value\":null},\"AppVersion\":{\"fieldtype\":\"hiddendata\",\"name\":\"AppVersion\",\"caption\":\"Release #:\",\"value\":\"8.0.642\"},\"AudaVinPayeeID\":{\"fieldtype\":\"hiddendata\",\"name\":\"AudaVinPayeeID\",\"caption\":\"{AudaVinPayeeID}\",\"value\":null},\"ClaimNum\":{\"fieldtype"
			"\":\"hiddendata\",\"name\":\"ClaimNum\",\"caption\":\"{ClaimNum}\",\"value\":\"LT_AS_{ClaimRandNum}\"},\"ScheduleVIN\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"size\":\"22\",\"maxlength\":\"17\",\"datatype\":\"string\",\"name\":\"ScheduleVIN\",\"caption\":\"{ScheduleVIN}\",\"value\":null},\"VIN\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"size\":\"22\",\"maxlength\":\"17\",\"style\":\"font-size:30;text-transform:uppercase\",\"datatype\":\"string\",\"onchange\":\"CallDecode('changeEvt'"
			")\",\"onkeypress\":\"return isAlphaNumeric(event)\",\"onpaste\":\"onVinPaste(this);\",\"name\":\"VIN\",\"caption\":\"VIN Selection\",\"value\":\"1HGCR3F85FA01{VINRand1}{VINRand2}\"},\"VINReasonCode\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"onchange\":\"VinReason()\",\"name\":\"VINReasonCode\",\"caption\":\"Reason For No VIN\",\"combovalue\":[{\"@value\":\"0\"},{\"@value\":\"1\",\"#text\":\"Inaccessible VIN\"},{\"@value\":\"2\",\"#text\":\"Unreadable VIN\"},{\"@value\":\"3\",\"#text\":\"None\"}],\""
			"value\":\"0\"},\"Label_VehDescription\":[{\"name\":\"Label_VehDescription\",\"caption\":\"Desc:\"},{\"name\":\"Label_VehDescription\",\"caption\":\"Desc:\"}],\"VehicleDescription1\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"static\",\"name\":\"VehicleDescription1\",\"caption\":\"Vehicle Description1:\",\"value\":\"{Veh_Desc1}\"},\"VehicleDescription2\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"static\",\"name\":\"VehicleDescription2\",\"caption\":\"Vehicle Description2:\",\"value\":\"{Veh_Desc2}\"},\"Vehicle_Engine\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"onchange\":\"OnEngine()\",\"style\":\"width:172\",\"name\":\"Vehicle_Engine\",\"caption\":\"Engine:\",\"value\":\"6cyl Gasoline 3.6\"},\"Label_Inspection\":{\"name\":\"Label_Inspection\",\"caption\":\"Inspection VIN\",\"value\":null},\"Vehicle_Transmission\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"onchange\":\"OnTransmission()\",\"style\":\"width:170\",\"name\":\"Vehicle_Transmission\",\"caption\":"
			"\"Transmission:\",\"value\":\"{TransText}\"},\"Vehicle_ImpactPoint\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"size\":\"3\",\"name\":\"Vehicle_ImpactPoint\",\"caption\":\"Point of Impact\",\"value\":null},\"ImpactPrimary\":{\"fieldtype\":\"hiddendata\",\"fieldset\":\"Estimate\",\"name\":\"ImpactPrimary\",\"caption\":\"Primary:\",\"value\":\"12\"},\"ImpactSecondary\":{\"fieldtype\":\"hiddendata\",\"fieldset\":\"Estimate\",\"name\":\"ImpactSecondary\",\"caption\":\"Secondary:\",\""
			"value\":\"11\"},\"TotalLossIndicator\":{\"fieldtype\":\"checkbox_flag\",\"fieldset\":\"Estimate\",\"OnClick\":\"totalLossIndClicked(this)\",\"name\":\"TotalLossIndicator\",\"caption\":\"Total Loss Indicator\",\"value\":\"1\"},\"BorderlineTotalLossIndicator\":{\"fieldtype\":\"checkbox_flag\",\"fieldset\":\"Estimate\",\"OnClick\":\"totalLossIndClicked(this)\",\"name\":\"BorderlineTotalLossIndicator\",\"caption\":\"Borderline\",\"value\":null},\"LowImpactFlag\":{\"fieldtype\":\"checkbox_flag\",\""
			"fieldset\":\"Estimate\",\"name\":\"LowImpactFlag\",\"caption\":\"Low Impact\",\"value\":\"N\"},\"Mileage\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"number\",\"maxlength\":\"7\",\"Format\":\"7.0\",\"size\":\"7\",\"name\":\"Mileage\",\"caption\":\"Mileage:\",\"value\":\"{TypicalMileage}\",\"mandatory\":\"*\"},\"Label_TypDesc\":{\"name\":\"Label_TypDesc\",\"caption\":\"Typical Mileage:\",\"value\":null},\"Typical\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"static\",\"size\":\"12\",\""
			"value\":null,\"name\":\"Typical\",\"caption\":\"Typical:\"},\"Actual\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:140\",\"name\":\"Actual\",\"caption\":\"Mileage Type:\",\"combovalue\":[{\"@value\":\"0\",\"#text\":\"Actual\"},{\"@value\":\"1\",\"#text\":\"Not Actual\"},{\"@value\":\"2\",\"#text\":\"Non Readable\"}],\"value\":\"0\"},\"Condition\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:100\",\"name\":\"Condition\",\"caption\":\"Condition"
			":\",\"combovalue\":[{\"@value\":\"0\"},{\"@value\":\"1\",\"#text\":\"Excellent\"},{\"@value\":\"2\",\"#text\":\"Good\"},{\"@value\":\"3\",\"#text\":\"Fair\"},{\"@value\":\"4\",\"#text\":\"Poor\"}],\"value\":\"2\"},\"LicenseNo\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"string\",\"style\":\"text-transform:uppercase\",\"maxlength\":\"10\",\"size\":\"12\",\"name\":\"LicenseNo\",\"caption\":\"Lic.Plate:\",\"value\":\"{PlateRand}PTP\"},\"LicenseState\":{\"fieldset\":\"Estimate\",\""
			"fieldtype\":\"statedropdown\",\"datatype\":\"string\",\"style\":\"width:160\",\"name\":\"LicenseState\",\"caption\":\"Lic State:\",\"combovalue\":[{\"@value\":\"\"},{\"@value\":\"AL\",\"#text\":\"Alabama\"},{\"@value\":\"AK\",\"#text\":\"Alaska\"},{\"@value\":\"AZ\",\"#text\":\"Arizona\"},{\"@value\":\"AR\",\"#text\":\"Arkansas\"},{\"@value\":\"CA\",\"#text\":\"California\"},{\"@value\":\"CO\",\"#text\":\"Colorado\"},{\"@value\":\"CT\",\"#text\":\"Connecticut\"},{\"@value\":\"DE\",\"#text\":\""
			"Delaware\"},{\"@value\":\"DC\",\"#text\":\"District of Columbia\"},{\"@value\":\"FL\",\"#text\":\"Florida\"},{\"@value\":\"GA\",\"#text\":\"Georgia\"},{\"@value\":\"GU\",\"#text\":\"Guam\"},{\"@value\":\"HI\",\"#text\":\"Hawaii\"},{\"@value\":\"ID\",\"#text\":\"Idaho\"},{\"@value\":\"IL\",\"#text\":\"Illinois\"},{\"@value\":\"IN\",\"#text\":\"Indiana\"},{\"@value\":\"IA\",\"#text\":\"Iowa\"},{\"@value\":\"KS\",\"#text\":\"Kansas\"},{\"@value\":\"KY\",\"#text\":\"Kentucky\"},{\"@value\":\"LA\",\""
			"#text\":\"Louisiana\"},{\"@value\":\"ME\",\"#text\":\"Maine\"},{\"@value\":\"MD\",\"#text\":\"Maryland\"},{\"@value\":\"MA\",\"#text\":\"Massachusetts\"},{\"@value\":\"MI\",\"#text\":\"Michigan\"},{\"@value\":\"MN\",\"#text\":\"Minnesota\"},{\"@value\":\"MS\",\"#text\":\"Mississippi\"},{\"@value\":\"MO\",\"#text\":\"Missouri\"},{\"@value\":\"MT\",\"#text\":\"Montana\"},{\"@value\":\"NE\",\"#text\":\"Nebraska\"},{\"@value\":\"NV\",\"#text\":\"Nevada\"},{\"@value\":\"NH\",\"#text\":\"New Hampshire\""
			"},{\"@value\":\"NJ\",\"#text\":\"New Jersey\"},{\"@value\":\"NM\",\"#text\":\"New Mexico\"},{\"@value\":\"NY\",\"#text\":\"New York\"},{\"@value\":\"NC\",\"#text\":\"North Carolina\"},{\"@value\":\"ND\",\"#text\":\"North Dakota\"},{\"@value\":\"OH\",\"#text\":\"Ohio\"},{\"@value\":\"OK\",\"#text\":\"Oklahoma\"},{\"@value\":\"OR\",\"#text\":\"Oregon\"},{\"@value\":\"PA\",\"#text\":\"Pennsylvania\"},{\"@value\":\"PR\",\"#text\":\"Puerto Rico\"},{\"@value\":\"RI\",\"#text\":\"Rhode Island\"},{\""
			"@value\":\"SC\",\"#text\":\"South Carolina\"},{\"@value\":\"SD\",\"#text\":\"South Dakota\"},{\"@value\":\"TN\",\"#text\":\"Tennessee\"},{\"@value\":\"TX\",\"#text\":\"Texas\"},{\"@value\":\"UT\",\"#text\":\"Utah\"},{\"@value\":\"VT\",\"#text\":\"Vermont\"},{\"@value\":\"VA\",\"#text\":\"Virginia\"},{\"@value\":\"VI\",\"#text\":\"Virgin Islands\"},{\"@value\":\"WA\",\"#text\":\"Washington\"},{\"@value\":\"WV\",\"#text\":\"West Virginia\"},{\"@value\":\"WI\",\"#text\":\"Wisconsin\"},{\"@value\":\""
			"WY\",\"#text\":\"Wyoming\"}],\"value\":\"TX\"},\"LicenseExpDateTime_Date\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"monthyeardate\",\"name\":\"LicenseExpDateTime_Date\",\"caption\":\"Lic Expire:\",\"value\":null},\"TaxExempt\":{\"fieldtype\":\"checkbox_flag\",\"fieldset\":\"Estimate\",\"name\":\"TaxExempt\",\"caption\":\"Tax Exempt\",\"value\":null},\"OwnerRetainedVehicle\":{\"fieldtype\":\"checkbox_flag\",\"fieldset\":\"Estimate\",\"name\":\"OwnerRetainedVehicle\",\""
			"caption\":\"Owner Retained Vehicle\",\"value\":null},\"VehInspNo\":{\"@custom\":\"1\",\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"string\",\"style\":\"text-transform:uppercase\",\"maxlength\":\"10\",\"size\":\"12\",\"name\":\"VehInspNo\",\"caption\":\"Veh Insp# :\",\"value\":null},\"VehProductionDateTime_Date\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"monthyeardate\",\"name\":\"VehProductionDateTime_Date\",\"caption\":\"Prod Date:\",\"value\":null},\""
			"PXNSearchArea\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"size\":\"25\",\"name\":\"PXNSearchArea\",\"caption\":\"Alternate Parts Search Area:\",\"value\":\"Geo 94583\"},\"PXSSearchArea\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"size\":\"25\",\"name\":\"PXSSearchArea\",\"caption\":\"Regional Recycled Average Price Search Area:\",\"value\":\"Default\"},\"PxnAvailableFlag\":{\"name\":\"PxnAvailableFlag\",\"caption\":\"{PxnAvailableFlag}\",\"value\":\"1\"},\"PxnSuppIncFlag\":{\""
			"name\":\"PxnSuppIncFlag\",\"caption\":\"{PxnSuppIncFlag}\",\"value\":\"61\"},\"PxnResearchMethod\":{\"name\":\"PxnResearchMethod\",\"caption\":\"{PxnResearchMethod}\",\"value\":null},\"RegionalPriceFlag\":{\"name\":\"RegionalPriceFlag\",\"caption\":\"{RegionalPriceFlag}\",\"value\":\"1\"},\"RSDLFlag\":{\"fieldtype\":\"hiddendata\",\"name\":\"RSDLFlag\",\"caption\":\"{RSDLFlag}\",\"value\":\"0\"},\"RSDLMode\":{\"fieldtype\":\"hiddendata\",\"name\":\"RSDLMode\",\"caption\":\"{RSDLMode}\",\"value\""
			":null},\"RSDLAdxId\":{\"fieldtype\":\"hiddendata\",\"name\":\"RSDLAdxId\",\"caption\":\"{RSDLAdxId}\",\"value\":null},\"RSDLInvMsg\":{\"fieldtype\":\"hiddendata\",\"name\":\"RSDLInvMsg\",\"caption\":\"{RSDLInvMsg}\",\"value\":\"0\"},\"PxsAutoSearchFlag\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"hiddendata\",\"name\":\"PxsAutoSearchFlag\",\"caption\":\"Use Regional Recycled Average Price for this estimate\",\"value\":\"1\"},\"DmgCount\":{\"name\":\"DmgCount\",\"caption\":\"{DmgCount}\",\"value\""
			":\"0\"},\"VehId\":{\"fieldset\":\"Estimate\",\"name\":\"VehId\",\"caption\":\"{VehId}\",\"value\":\"{VID_captured2}\"},\"Guid\":{\"fieldset\":\"Estimate\",\"name\":\"Guid\",\"caption\":\"{Guid}\",\"value\":null},\"Packages\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"size\":\"5\",\"style\":\"width:330\",\"onclick\":\"formVehicle.SPackages.selectedIndex=-1; showPkgDetail(formVehicle.Packages,formVehicle.SPackages,\\\"Pack\\\");\",\""
			"ondblclick\":\"packcheck(formVehicle.Packages,formVehicle.SPackages,formVehicle.Options,formVehicle.Selopt);swapper(formVehicle.Packages,formVehicle.SPackages,1,formVehicle.Options,formVehicle.Selopt,p_o);showPkgDetail(formVehicle.Packages,formVehicle.SPackages,\\\"Pack\\\")\",\"onmouseenter\":\"myOnMouseEnter()\",\"name\":\"Packages\",\"caption\":\"Packages\",\"value\":null},\"PackagesDesc\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"textarea\",\"rows\":\"5\",\"style\":\"width:430\",\"name\":\""
			"PackagesDesc\",\"caption\":\"Package Description\",\"value\":null},\"Options\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"size\":\"5\",\"style\":\"width:330\",\"onclick\":\"formVehicle.Selopt.selectedIndex=-1\",\"multiple\":\"yes\",\"ondblclick\":\"swapcheck(formVehicle.Options,formVehicle.Selopt);\",\"onmouseenter\":\"myOnMouseEnter()\",\"name\":\"Options\",\"caption\":\"Options\",\"value\":\"Anti-Lock BrakesAir ConditioningAutomatic Dimming MirrorAuto Load LevelingAlarm "
			"SystemAluminum/Alloy WheelsBodyside CladdingCruise ControlAM/FM CD PlayerCenter ConsoleDual AirbagsRear Window DefrosterHeated Power MirrorsDual Zone Auto A/CPower Drivers SeatFog LightsHead AirbagsIntermittent WipersKeyless Entry SystemLighted Entry SystemLeather Steering WheelLeather SeatsOverhead ConsoleOnStar SystemPower BrakesPower Door LocksPrivacy GlassPower SteeringPower WindowsRoof/Luggage RackReverse Sensing SystemRem Trunk-L/Gate ReleaseRear Window Wiper/WasherSide AirbagsStability "
			"Cntrl SuspensnStrg Wheel Radio ControlTachometerTraction Control SystemTheft Deterrent SystemTonneau/Cargo CoverTinted GlassTilt Steering Wheel\"},\"AudaVinSelected\":{\"fieldtype\":\"hiddendata\",\"name\":\"AudaVinSelected\",\"caption\":\"{AudaVinSelected}\",\"value\":null},\"Text_bottom\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"static\",\"name\":\"Text_bottom\",\"caption\":\"{Text_bottom}\",\"value\":null},\"GenericFlag\":{\"fieldtype\":\"checkbox_flag\",\"name\":\"GenericFlag\",\"caption\":"
			"\"None\",\"value\":null},\"Button_vin\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"button\",\"onclick\":\"GoToLayer(1)\",\"style\":\"width:120\",\"name\":\"Button_vin\",\"caption\":\"Vehicle\",\"value\":null},\"Button_point\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"button\",\"onclick\":\"GoToLayer(2)\",\"style\":\"width:120\",\"name\":\"Button_point\",\"caption\":\"Point of Impact\",\"value\":null},\"Button_equip\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"button\",\"onclick\":\"GoToLayer(3)\""
			",\"style\":\"width:120\",\"name\":\"Button_equip\",\"caption\":\"Options\",\"value\":null},\"Label_VehSelected\":{\"name\":\"Label_VehSelected\",\"caption\":\"Vehicle Selection\",\"value\":null},\"Label_VehNotListed\":{\"name\":\"Label_VehNotListed\",\"caption\":\"Vehicle Not Listed\",\"value\":null},\"Label_Specialty\":{\"name\":\"Label_Specialty\",\"caption\":\"Specialty\",\"value\":null},\"Origin\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:130\",\"onchange\":\""
			"OnRegion()\",\"size\":\"1\",\"name\":\"Origin\",\"caption\":\"Origin:\",\"combovalue\":[{\"@value\":\"\"},{\"@value\":\"North American\",\"#text\":\"North American\"},{\"@value\":\"Asian\",\"#text\":\"Asian\"},{\"@value\":\"European\",\"#text\":\"European\"}],\"value\":null},\"Makes\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:160\",\"onchange\":\"OnMake()\",\"size\":\"1\",\"name\":\"Makes\",\"caption\":\"Make:\",\"value\":null},\"Year\":{\"fieldset\":\"Estimate\",\""
			"fieldtype\":\"dropdown\",\"style\":\"width:80\",\"onchange\":\"OnYear()\",\"size\":\"1\",\"name\":\"Year\",\"caption\":\"Year:\",\"value\":null},\"Model\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:140\",\"onchange\":\"OnModel()\",\"size\":\"1\",\"name\":\"Model\",\"caption\":\"Model:\",\"value\":null},\"Style\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:100%\",\"onchange\":\"OnStyle()\",\"size\":\"1\",\"name\":\"Style\",\"caption\":\"Style"
			":\",\"value\":null},\"VehNotLisYear\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:80\",\"size\":\"1\",\"onchange\":\"Generic('year')\",\"name\":\"VehNotLisYear\",\"caption\":\"Year:\",\"value\":null},\"VehNotLisStyle\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:150\",\"size\":\"1\",\"onchange\":\"Generic('style')\",\"name\":\"VehNotLisStyle\",\"caption\":\"Style:\",\"value\":null},\"VehNotLisDesc\":{\"fieldset\":\"Estimate\",\"fieldtype\":\""
			"input\",\"size\":\"34\",\"maxlength\":\"34\",\"datatype\":\"string\",\"onchange\":\"Generic('tdesc')\",\"name\":\"VehNotLisDesc\",\"caption\":\"Description:\",\"value\":null},\"VehNotLisTrans\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:160\",\"size\":\"1\",\"onchange\":\"Generic('trans')\",\"name\":\"VehNotLisTrans\",\"caption\":\"Transmission:\",\"value\":null},\"VehMileage\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"number\",\"maxlength\":\""
			"7\",\"Format\":\"7.0\",\"size\":\"7\",\"name\":\"VehMileage\",\"caption\":\"Mileage:\",\"value\":null},\"VehActual\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:140\",\"name\":\"VehActual\",\"caption\":\"Mileage Type:\",\"combovalue\":[{\"@value\":\"0\",\"#text\":\"Actual\"},{\"@value\":\"1\",\"#text\":\"Not Actual\"},{\"@value\":\"2\",\"#text\":\"Non Readable\"}],\"value\":null},\"ExtrColor\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"string\",\""
			"maxlength\":\"80\",\"size\":\"50\",\"name\":\"ExtrColor\",\"caption\":\"Exterior Color:\",\"value\":null},\"IntrColor\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"string\",\"maxlength\":\"80\",\"size\":\"50\",\"name\":\"IntrColor\",\"caption\":\"Interior Color:\",\"value\":null},\"ExtrRefinish\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:170\",\"onchange\":\"OnExtr()\",\"size\":\"1\",\"useDefault\":\"1\",\"name\":\"ExtrRefinish\",\"caption\":\""
			"Exterior Refinish:\",\"combovalue\":[{\"@value\":0,\"#text\":\"Single-Stage\"},{\"@value\":1,\"#text\":\"Two-Stage\"},{\"@value\":2,\"#text\":\"Two-Stage User Defined\"}],\"value\":\"1\"},\"IntrRefinish\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:170\",\"size\":\"1\",\"useDefault\":\"0\",\"name\":\"IntrRefinish\",\"caption\":\"Interior Refinish:\",\"combovalue\":[{\"@value\":0,\"#text\":\"Single-Stage\"},{\"@value\":1,\"#text\":\"Two-Stage\"}],\"value\":\"0\"},\""
			"ExtrPaintCode\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"string\",\"size\":\"15\",\"maxlength\":\"15\",\"name\":\"ExtrPaintCode\",\"caption\":\"Exterior Paintcode:\",\"value\":null},\"IntrPaintCode\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"string\",\"size\":\"15\",\"maxlength\":\"15\",\"name\":\"IntrPaintCode\",\"caption\":\"Interior Trim:\",\"value\":null},\"Button_pre81\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"button\",\"onclick\":\""
			"GoToLayer(4)\",\"style\":\"width:112\",\"name\":\"Button_pre81\",\"caption\":\"Pre 81\",\"value\":null},\"Pre81_VIN\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"style\":null,\"name\":\"Pre81_VIN\",\"caption\":\"VIN:\",\"value\":null},\"Pre81_Type\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:160\",\"size\":\"1\",\"onchange\":\"OnPre81('type')\",\"name\":\"Pre81_Type\",\"caption\":\"Type:\",\"combovalue\":[{\"@value\":\"0\"},{\"@value\":\"1\",\"#text\":\"Car\"}"
			",{\"@value\":\"2\",\"#text\":\"Truck\"},{\"@value\":\"3\",\"#text\":\"MotorCycle\"}],\"value\":null},\"Pre81_Origin\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:130\",\"size\":\"1\",\"onchange\":\"OnPre81('origin')\",\"name\":\"Pre81_Origin\",\"caption\":\"Origin:\",\"combovalue\":[{\"@value\":\"\"},{\"@value\":\"North American\",\"#text\":\"North American\"},{\"@value\":\"Asian\",\"#text\":\"Asian\"},{\"@value\":\"European\",\"#text\":\"European\"}],\"value\":null},\""
			"Pre81_Make\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:160\",\"size\":\"1\",\"onchange\":\"OnPre81('make')\",\"name\":\"Pre81_Make\",\"caption\":\"Make:\",\"value\":null},\"Pre81_Year\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:80\",\"size\":\"1\",\"onchange\":\"OnPre81('year')\",\"name\":\"Pre81_Year\",\"caption\":\"Year:\",\"value\":null},\"Button_pre81_Decode\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"button\",\"onclick\":\""
			"Pre81Decode()\",\"style\":\"width:60\",\"name\":\"Button_pre81_Decode\",\"caption\":\"Decode\",\"value\":null},\"Button_pre81_Cancel\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"button\",\"onclick\":\"GoToLayer(1)\",\"style\":\"width:60\",\"name\":\"Button_pre81_Cancel\",\"caption\":\"Cancel\",\"value\":null},\"VinSel_Select\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"size\":\"12\",\"style\":\"width:100%;\",\"onclick\":\"Enable()\",\"name\":\"VinSel_Select\",\"caption\":\""
			"{VinSel_Select}\",\"value\":null},\"Button_VinSel_Select\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"button\",\"style\":\"width:60\",\"onclick\":\"VehInfo()\",\"name\":\"Button_VinSel_Select\",\"caption\":\"Select\",\"value\":null},\"Button_VinSel_Cancel\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"button\",\"onclick\":\"DisbFields()\",\"style\":\"width:60\",\"name\":\"Button_VinSel_Cancel\",\"caption\":\"Cancel\",\"value\":null},\"Button_Copy\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"button\""
			",\"onclick\":\"ValCopy()\",\"style\":\"width:60\",\"name\":\"Button_Copy\",\"caption\":\"Copy>>\",\"value\":null},\"Button_Decode\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"button\",\"onclick\":\"CallDecode('clickEvt')\",\"style\":\"width:112\",\"name\":\"Button_Decode\",\"caption\":\"Decode\",\"value\":null},\"Button_AudaVINDecode\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"button\",\"onclick\":\"CallDecode('AudaVIN')\",\"style\":\"width:112\",\"name\":\"Button_AudaVINDecode\",\"caption\":\""
			"AudaVIN\",\"value\":null},\"Button_DetectConn\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"button\",\"onclick\":\"DetectConnection()\",\"style\":\"width:120\",\"name\":\"Button_DetectConn\",\"caption\":\"Detect Connection\",\"value\":null},\"DefVehPriorDamage\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"checkbox_flag\",\"OnClick\":\"OnVehPriorDamage()\",\"name\":\"DefVehPriorDamage\",\"caption\":\"Default to Gross Total in Prior Damage Estimate\",\"value\":\"N\"},\"VehPriorDamage\":{\"fieldset\":"
			"\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"currency\",\"name\":\"VehPriorDamage\",\"caption\":\"Prior Damage Estimate Gross Total:\",\"value\":\"$0.00\"},\"PDDescription\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"textarea\",\"rows\":\"3\",\"cols\":\"80\",\"name\":\"PDDescription\",\"caption\":\"Description:\",\"value\":null},\"VehUnDocPriorDamage\":{\"fieldtype\":\"checkbox_flag\",\"fieldset\":\"Estimate\",\"OnClick\":\"OnClickPDNotExist()\",\"name\":\"VehUnDocPriorDamage\",\"caption\""
			":\"Prior damage exists:\",\"value\":\"N\"},\"EstimateActiveFlag\":{\"fieldtype\":\"hiddendata\",\"name\":\"EstimateActiveFlag\",\"caption\":\"{EstimateActiveFlag}\",\"value\":\"1\"},\"AutoSourceActiveFlag\":{\"fieldtype\":\"hiddendata\",\"name\":\"AutoSourceActiveFlag\",\"caption\":\"{AutoSourceActiveFlag}\",\"value\":\"1\"},\"ValOnlyActiveFlag\":{\"fieldtype\":\"hiddendata\",\"name\":\"ValOnlyActiveFlag\",\"caption\":\"{ValOnlyActiveFlag}\",\"value\":\"0\"},\"VINStatus\":{\"fieldtype\":\""
			"hiddendata\",\"name\":\"VINStatus\",\"caption\":\"{VINStatus}\",\"value\":\"{VIN_Status}\"},\"TaxExemptFlag\":{\"fieldtype\":\"hiddendata\",\"name\":\"TaxExemptFlag\",\"caption\":\"{TaxExemptFlag}\",\"value\":null},\"OwnerRetianedVehicleFlag\":{\"fieldtype\":\"hiddendata\",\"name\":\"OwnerRetianedVehicleFlag\",\"caption\":\"{OwnerRetianedVehicleFlag}\",\"value\":null},\"AudaVinRefinishOverride\":{\"fieldtype\":\"hiddendata\",\"name\":\"AudaVinRefinishOverride\",\"caption\":\"{AudaVinRefinishOverride}\",\""
			"value\":\"1\"},\"TypicalValue\":{\"fieldtype\":\"hiddendata\",\"fieldset\":\"Estimate\",\"name\":\"TypicalValue\",\"caption\":\"{TypicalValue}\",\"value\":\"490900\"},\"TwoStageMaxCntl\":{\"fieldtype\":\"hiddendata\",\"fieldset\":\"PayerIns\",\"name\":\"TwoStageMaxCntl\",\"caption\":\"{TwoStageMaxCntl}\",\"value\":\"1\"},\"ThreeStageMaxCntl\":{\"fieldtype\":\"hiddendata\",\"fieldset\":\"PayerIns\",\"name\":\"ThreeStageMaxCntl\",\"caption\":\"{ThreeStageMaxCntl}\",\"value\":\"0\"},\"RecallFlag\":{"
			"\"fieldtype\":\"hiddendata\",\"name\":\"RecallFlag\",\"caption\":\"{RecallFlag}\",\"value\":\"1\"},\"FrameFlag\":{\"fieldtype\":\"hiddendata\",\"name\":\"FrameFlag\",\"caption\":\"{FrameFlag}\",\"value\":\"0\"},\"CrashFlag\":{\"fieldtype\":\"hiddendata\",\"name\":\"CrashFlag\",\"caption\":\"{CrashFlag}\",\"value\":\"0\"},\"Veh_LossDate\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"hiddendata\",\"datatype\":\"date\",\"name\":\"Veh_LossDate\",\"caption\":\"{Veh_LossDate}\",\"value\":\"{LossDate}\"},"
			"\"Market_ZipCode\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"hiddendata\",\"datatype\":\"postal\",\"name\":\"Market_ZipCode\",\"caption\":\"{Market_ZipCode}\",\"value\":\"{ZipCode}-\"},\"Orig_LossDate\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"hiddendata\",\"datatype\":\"date\",\"name\":\"Orig_LossDate\",\"caption\":\"{Orig_LossDate}\",\"value\":\"{LossDate}\"},\"Owner_StateCode\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"hiddendata\",\"name\":\"Owner_StateCode\",\"caption\":\"{Owner_StateCode}\","
			"\"value\":null},\"TypicalMarketValue\":{\"fieldtype\":\"hiddendata\",\"fieldset\":\"Estimate\",\"name\":\"TypicalMarketValue\",\"caption\":\"{TypicalMarketValue}\",\"value\":null},\"TypicalTypeIndicator\":{\"fieldtype\":\"hiddendata\",\"fieldset\":\"Estimate\",\"name\":\"TypicalTypeIndicator\",\"caption\":\"{TypicalTypeIndicator}\",\"value\":null},\"OdometerAdjust\":{\"fieldtype\":\"hiddendata\",\"fieldset\":\"Estimate\",\"name\":\"OdometerAdjust\",\"caption\":\"{OdometerAdjust}\",\"value\":null},"
			"\"PxnMileage\":{\"fieldtype\":\"hiddendata\",\"name\":\"PxnMileage\",\"caption\":\"{PxnMileage}\",\"value\":\"\"},\"PDEstFlag\":{\"fieldtype\":\"hiddendata\",\"name\":\"PDEstFlag\",\"caption\":\"{PDEstFlag}\",\"value\":\"0\"},\"UpdPDEstFlag\":{\"fieldtype\":\"hiddendata\",\"name\":\"UpdPDEstFlag\",\"caption\":\"{UpdPDEstFlag}\",\"value\":\"0\"},\"Item_Type\":{\"fieldtype\":\"hiddendata\",\"name\":\"Item_Type\",\"caption\":\"{Item_Type}\",\"value\":\"E\"},\"LinkedItem\":{\"fieldtype\":\"hiddendata"
			"\",\"name\":\"LinkedItem\",\"caption\":\"{LinkedItem}\",\"value\":null},\"EstStatus\":{\"fieldtype\":\"hiddendata\",\"name\":\"EstStatus\",\"caption\":\"{EstStatus}\",\"value\":\"0\"},\"PDExists\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"radio\",\"name\":\"PDExists\",\"caption\":\"Linked prior damage estimate exists\",\"value\":null},\"PDNotExist\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"checkbox_flag\",\"OnClick\":\"OnClickPDNotExist()\",\"name\":\"PDNotExist\",\"caption\":\"Prior damage "
			"exists\",\"value\":null},\"PriorEstType\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"hiddendata\",\"name\":\"PriorEstType\",\"caption\":\"{PriorEstType}\",\"value\":\"0\"},\"PrevLink\":{\"name\":\"PrevLink\",\"caption\":\"{PrevLink}\",\"value\":null},\"ProfileId\":{\"fieldtype\":\"hiddendata\",\"name\":\"ProfileId\",\"caption\":\"{ProfileId}\",\"value\":\"{Profile_Id}\"},\"AiMExportPath\":{\"fieldtype\":\"hiddendata\",\"name\":\"AiMExportPath\",\"caption\":\"{AiMExportPath}\",\"value\":null},\""
			"TLReinspectionMode\":{\"fieldtype\":\"hiddendata\",\"name\":\"TLReinspectionMode\",\"caption\":\"{TLReinspectionMode}\",\"value\":\"N\"},\"OfflineMode\":{\"name\":\"OfflineMode\",\"caption\":\"{OfflineMode}\",\"value\":\"0\"},\"AudaVinConnected\":{\"fieldtype\":\"hiddendata\",\"name\":\"AudaVinConnected\",\"caption\":\"{AudaVinConnected}\",\"value\":\"0\"},\"AudaVinMakeList\":{\"fieldtype\":\"hiddendata\",\"name\":\"AudaVinMakeList\",\"caption\":\"{AudaVinMakeList}\",\"value\":null},\""
			"OEMPartPriceFlag\":{\"fieldtype\":\"hiddendata\",\"name\":\"OEMPartPriceFlag\",\"caption\":\"{OEMPartPriceFlag}\",\"value\":\"1\"},\"IsPQEVehicle\":{\"fieldtype\":\"hiddendata\",\"name\":\"IsPQEVehicle\",\"caption\":\"{IsPQEVehicle}\",\"value\":\"1\"},\"IsOEMPartPriceFileExist\":{\"fieldtype\":\"hiddendata\",\"name\":\"IsOEMPartPriceFileExist\",\"caption\":\"{IsOEMPartPriceFileExist}\",\"value\":\"0\"},\"IsAnyDMIProductExist\":{\"fieldtype\":\"hiddendata\",\"name\":\"IsAnyDMIProductExist\",\""
			"caption\":\"{IsAnyDMIProductExist}\",\"value\":null},\"SpecialtyVIN\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"size\":\"22\",\"maxlength\":\"17\",\"style\":\"font-size:30;text-transform:uppercase\",\"datatype\":\"string\",\"name\":\"SpecialtyVIN\",\"caption\":\"{SpecialtyVIN}\",\"value\":\"1HGCR3F85FA01{VINRand1}{VINRand2}\"},\"VehChanged\":{\"fieldtype\":\"hiddendata\",\"name\":\"VehChanged\",\"caption\":\"{VehChanged}\",\"value\":null},\"SpecialtyVehCategory\":{\"fieldset\":\"Estimate\",\"fieldtype"
			"\":\"dropdown\",\"style\":\"width:160\",\"size\":\"1\",\"onchange\":\"ASVehCategoryChange(formVehicle)\",\"name\":\"SpecialtyVehCategory\",\"caption\":\"Category:\",\"value\":null},\"SpecialtyVehType\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:160\",\"size\":\"1\",\"onchange\":\"ASVehTypeChange(formVehicle)\",\"name\":\"SpecialtyVehType\",\"caption\":\"Vehicle Type:\",\"value\":null},\"SpecialtySpecificVeh\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\""
			"style\":\"width:160\",\"size\":\"1\",\"onchange\":\"ASSpecificVehChange(formVehicle)\",\"name\":\"SpecialtySpecificVeh\",\"caption\":\"Specific Vehicle:\",\"value\":null},\"ReqReady\":\"1\",\"ClaimNo\":{\"@custom\":\"1\",\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"claimnumber\",\"size\":\"20\",\"CustId\":\"APL\",\"mask\":\"\",\"maskEmpty\":\"\",\"maskSize\":\"25\",\"name\":\"ClaimNo\",\"caption\":\"Claim # :\",\"value\":\"LT_AS_{ClaimRandNum}\",\"mandatory\":\"*\"},\"EstimateStatus\":"
			"{\"fieldset\":\"Estimate\",\"fieldtype\":\"hiddendata\",\"name\":\"EstimateStatus\",\"caption\":\"Status:\",\"combovalue\":[{\"@value\":\"0\",\"#text\":\"Assigned\"},{\"@value\":\"1\",\"#text\":\"In Progress\"},{\"@value\":\"2\",\"#text\":\"Completed Estimate Only\"},{\"@value\":\"3\",\"#text\":\"Completed Estimate, Notes, Forms\"},{\"@value\":\"4\",\"#text\":\"Template\"},{\"@value\":\"5\",\"#text\":\"Assgn Dup (Assignment Duplicate)\"},{\"@value\":\"6\",\"#text\":\"Assgn Comp (Assignment "
			"Completed)\"},{\"@value\":\"7\",\"#text\":\"Ready\"},{\"@value\":\"8\",\"#text\":\"Est Dup (Estimate Duplicate)\"},{\"@value\":\"9\",\"#text\":\"Report\"},{\"@value\":\"10\",\"#text\":\"Assgn Supp\"},{\"@value\":\"11\",\"#text\":\"Cancelled\"},{\"@value\":\"12\",\"#text\":\"Pending Approval\"},{\"@value\":\"13\",\"#text\":\"Not Approved\"},{\"@value\":\"14\",\"#text\":\"Approved\"},{\"@value\":\"15\",\"#text\":\"Rev Assigned\"},{\"@value\":\"16\",\"#text\":\"Update In Progress\"},{\"@value\":\"17"
			"\",\"#text\":\"Agreement Pending\"},{\"@value\":\"18\",\"#text\":\"Update Completed\"},{\"@value\":\"19\",\"#text\":\"Completed with Revisions\"},{\"@value\":\"20\",\"#text\":\"Assigned Reinspection\"},{\"@value\":\"21\",\"#text\":\"In Progress Reinspection\"},{\"@value\":\"22\",\"#text\":\"Completed as Reference\"},{\"@value\":\"23\",\"#text\":\"Imported\"},{\"@value\":\"24\",\"#text\":\"Completed as Record\"},{\"@value\":\"25\",\"#text\":\"Sent for Reinspection\"},{\"@value\":\"26\",\"#text\":\""
			"Retrieved\"},{\"@value\":\"27\",\"#text\":\"Return Review\"},{\"@value\":\"28\",\"#text\":\"Supp Notice\"}],\"value\":\"0\"},\"VehicleDescription\":{\"fieldset\":\"Estimate\",\"datatype\":\"string\",\"name\":\"VehicleDescription\",\"caption\":\"Vehicle Description:\",\"value\":\"{Veh_Desc1}\"},\"ValuationStatus\":{\"fieldtype\":\"statictrans\",\"name\":\"ValuationStatus\",\"caption\":\"Status:\",\"combovalue\":[{\"@value\":\"0\",\"#text\":\"In Progress\"},{\"@value\":\"1\",\"#text\":\"In "
			"Progress\"},{\"@value\":\"3\",\"#text\":\"Completed\"},{\"@value\":\"7\",\"#text\":\"Ready\"},{\"@value\":\"WIP\",\"#text\":\"Work In Progress\"},{\"@value\":\"COM\",\"#text\":\"Completed\"},{\"@value\":\"CAN\",\"#text\":\"Cancelled\"},{\"@value\":\"RCV\",\"#text\":\"Received\"},{\"@value\":\"FOR\",\"#text\":\"Forwarded\"}],\"value\":\"1\"},\"Check_CommitValuation\":{\"fieldtype\":\"checkbox_single\",\"OnClick\":\"onSelectReady()\",\"name\":\"Check_CommitValuation\",\"caption\":\"Request Ready\","
			"\"value\":null},\"ProdDescription\":{\"fieldtype\":\"static\",\"name\":\"ProdDescription\",\"caption\":\"Product Desc:\",\"value\":\"Test Prod For TLDG\"},\"ShoplinkLiteVal\":{\"fieldtype\":\"hiddendata\",\"name\":\"ShoplinkLiteVal\",\"caption\":\"{ShoplinkLiteVal}\",\"value\":\"0\"},\"GUID\":{\"name\":\"GUID\",\"caption\":\"{GUID}\",\"value\":\"{ClaimGUID}\"},\"BaseXML\":\"Vehicle.XML\",\"HidMileage\":\"{TypicalMileage}\",\"Select\":{\"value\":\"Geo 94583\"},\"HidEngine\":{\"name"
			"\":\"HidEngine\",\"value\":\"{EngineText}|{EngineCode}|{EngineNumber}\"},\"HidTransmission\":{\"name\":\"HidTransmission\",\"value\":\"{TransText}|{TransCode}\"},\"HidPackages\":{\"name\":\"HidPackages\",\"value\":\"\"},\"HidOptions\":{\"name\":\"HidOptions\",\"value\":\"{HID_Options1}\"}}}", 
			LAST);
	
//	lr_end_transaction("AS_Service_ClaimUpdate_4", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T11_ClickCondition_ST02_Service_ClaimUpdate", LR_AUTO);

	web_revert_auto_header("Origin");

//	lr_start_transaction("AS_AutosourceSettings_ASWebUrl");
	
//	lr_start_sub_transaction("RMS_AutoSource_T11_ClickCondition_ST03_AutosourceSettings_ASWebUrl1", "RMS_AutoSource_T11_ClickCondition");
//	
//		web_url("ASWebUrl_3", 
//			"URL={ADXE_URL}/api/AutosourceSettings/ASWebUrl", 
//			"Resource=0", 
//			"RecContentType=text/plain", 
//			"Referer={ADXE_URL}/Estimating/Autosource/Condition/IntExt", 
//			"Snapshot=t69.inf", 
//			"Mode=HTML", 
//			LAST);
//
////	lr_end_transaction("AS_AutosourceSettings_ASWebUrl", LR_AUTO);
//	
//	lr_end_sub_transaction("RMS_AutoSource_T11_ClickCondition_ST03_AutosourceSettings_ASWebUrl1", LR_AUTO);
//
////	lr_start_transaction("AS_AutosourceSettings_ASWebUrl");
//	
//	lr_start_sub_transaction("RMS_AutoSource_T11_ClickCondition_ST04_AutosourceSettings_ASWebUrl2", "RMS_AutoSource_T11_ClickCondition");
//	
//		web_url("ASWebUrl_4", 
//			"URL={ADXE_URL}/api/AutosourceSettings/ASWebUrl", 
//			"Resource=0", 
//			"RecContentType=text/plain", 
//			"Referer={ADXE_URL}/Estimating/Autosource/Condition/IntExt", 
//			"Snapshot=t70.inf", 
//			"Mode=HTML", 
//			LAST);
////	lr_end_transaction("AS_AutosourceSettings_ASWebUrl", LR_AUTO);
//	
//	lr_end_sub_transaction("RMS_AutoSource_T11_ClickCondition_ST04_AutosourceSettings_ASWebUrl2", LR_AUTO);
//	
//	
//	web_add_auto_header("Sec-Fetch-Site", "same-site");
//
//	web_add_header("Origin", "{ADXE_URL}");
//	
////	lr_start_transaction("AS_AutosourceWeb_urls");
//	
//	lr_start_sub_transaction("RMS_AutoSource_T11_ClickCondition_ST05_AutosourceWeb_urls", "RMS_AutoSource_T11_ClickCondition");
//	
//		web_url("urls_2", 
//			"URL={autoSource_URL}/urls", 
//			"Resource=0", 
//			"Referer={ADXE_URL}/Estimating/Autosource/Condition/IntExt", 
//			"Snapshot=t72.inf", 
//			"Mode=HTML", 
//			LAST);
////	lr_end_transaction("AS_AutosourceWeb_urls", LR_AUTO);
//	
//	lr_end_sub_transaction("RMS_AutoSource_T11_ClickCondition_ST05_AutosourceWeb_urls", LR_AUTO);
	
	web_add_auto_header("Sec-Fetch-Site", "same-origin");
	
//	lr_start_transaction("AS_Page_ASConditionIntExt");
	
	lr_start_sub_transaction("RMS_AutoSource_T11_ClickCondition_ST03_Page_ASConditionIntExt", "RMS_AutoSource_T11_ClickCondition");
	
		web_url("ASConditionIntExt", 
			"URL={ADXE_URL}/api/Page/ASConditionIntExt", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Condition/IntExt", 
			"Snapshot=t73.inf", 
			"Mode=HTML", 
			LAST);
//	lr_end_transaction("AS_Page_ASConditionIntExt", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T11_ClickCondition_ST03_Page_ASConditionIntExt", LR_AUTO);
	
lr_end_transaction("RMS_AutoSource_T11_ClickCondition",LR_AUTO);
	return 0;
}
