Login()
{
	
//	lr_think_time(TT);
	
lr_start_transaction("RMS_AutoSource_T01_Login");

	lr_start_sub_transaction("RMS_AutoSource_T01_Login_ST00_loginservices_getuserticket", "RMS_AutoSource_T01_Login");

			web_reg_save_param_ex("ParamName=platFormToken", "LB=platformtoken=", "RB=;",
				SEARCH_FILTERS,
				"Scope=HEADERS",
				LAST);
	
			web_custom_request("getuserticket", 
				"URL={ADXE_URL}/api/loginservices/getuserticket", 
				"Method=POST", 
				"Resource=0", 
				"RecContentType=text/plain", 
				"Referer={ADXE_URL}/Login/", 
				"Snapshot=t10.inf", 
				"Mode=HTML", 
				"EncType=application/json;charset=UTF-8", 
				"Body={\"uid\":\"{UserID}\",\"pwd\":\"{PassWord}\",\"dtm\":\"{LoginTime}\"}", 
				LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T01_Login_ST00_loginservices_getuserticket", LR_AUTO);

	lr_start_sub_transaction("RMS_AutoSource_T01_Login_ST01_loginservices_GetUserinfo", "RMS_AutoSource_T01_Login");
	
			web_reg_find("Fail=NotFound","Search=Body","Text=\"welcome\": \"Welcome",LAST);
			
			web_url("GetUserInfo", 
				"URL={ADXE_URL}/api/LoginServices/GetUserInfo", 
				"Resource=1", 
				"RecContentType=text/plain", 
				"Referer={ADXE_URL}/Estimating/WorkList", 
				"Snapshot=t11.inf", 
				LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T01_Login_ST01_loginservices_GetUserinfo", LR_AUTO);
	
	lr_start_sub_transaction("RMS_AutoSource_T01_Login_ST02_Page_ClaimsList", "RMS_AutoSource_T01_Login");
			
			web_reg_save_param_ex("ParamName=Cookie_1", "LB=ticket=", "RB=;",
				SEARCH_FILTERS,
				"Scope=HEADERS",
				LAST);
	
			web_reg_find("Fail=NotFound", "Search=Body", "Text=\"GUID\":{\"fieldtype\":\"static\",\"name\":\"GUID\",\"caption\":\"\{GUID\}\",\"value\":\"", LAST);
			
//		web_reg_save_param_regexp(
//		"ParamName=pkval1",
//		"RegExp=WA_PK\}\",\"value\":\"(.*?)\",\"text(.*?)EstimateStatusValue\}\",\"value\":\"9\"",
//		"Group=1",
//		"Ordinal=all",
//		SEARCH_FILTERS,
//		"Scope=BODY",
//		LAST);

			web_custom_request("ClaimsList",
				"URL={ADXE_URL}/api/Page/ClaimsList", 
				"Method=POST", 
				"Resource=0", 
				"RecContentType=application/json", 
				"Referer={ADXE_URL}/Estimating/WorkList", 
				"Snapshot=t12.inf", 
				"Mode=HTML", 
				"EncType=application/json;charset=UTF-8", 
				"Body={\"Form\":{\"ApptDateTime_Date\":\"\",\"ApptDateTime_Time\":\"\",\"Header_Filter\":\"\",\"Header_Filter_RO\":0,\"Header_Profiles\":\"All\",\"ItemsPerPage\":\"\",\"PageNo\":1,\"SortKey\":\"\"}}", 
				LAST);
	
	
	lr_end_sub_transaction("RMS_AutoSource_T01_Login_ST02_Page_ClaimsList", LR_AUTO);
	
	lr_end_transaction("RMS_AutoSource_T01_Login",LR_AUTO);	
	
lr_think_time(TT);
		
//	lr_start_sub_transaction("RMS_AutoSource_T01_Login_ST03_CiecaSettings_CiecaAclUrl", "RMS_AutoSource_T01_Login");
//			
//			web_reg_find("Fail=NotFound", "Search=Body", "Text=cieca-acl", LAST);
//			
//			web_custom_request("CiecaAclUrl", 
//				"URL={ADXE_URL}/api/CiecaSettings/CiecaAclUrl", 
//				"Method=GET", 
//				"Resource=0", 
//				"RecContentType=application/json, text/plain, */*", 
//				"Referer={ADXE_URL}/Estimating/WorkList", 
//				"Snapshot=t12.inf", 
//				"Mode=HTML", 
//				"EncType=application/json;charset=UTF-8", 
//				LAST);
//			
//	lr_end_sub_transaction("RMS_AutoSource_T01_Login_ST03_CiecaSettings_CiecaAclUrl", LR_AUTO);
//
//	
//	lr_start_sub_transaction("RMS_AutoSource_T01_Login_ST04_CiecaSettings_CiecaWebUrl", "RMS_AutoSource_T01_Login");
//			
//			web_url("CiecaWebUrl",
//				"URL={ADXE_URL}/api/CiecaSettings/CiecaWebUrl", 
//				"Resource=1", 
//				"RecContentType=text/plain", 
//				"Referer={ADXE_URL}/Estimating/WorkList", 
//				"Snapshot=t16.inf", 
//				LAST);
//		
//	lr_end_sub_transaction("RMS_AutoSource_T01_Login_ST04_CiecaSettings_CiecaWebUrl", LR_AUTO);
//
//	Cieca_import();
		

	lr_start_transaction("RMS_AutoSource_T02_SortClaimList");
	
			web_reg_find("Fail=NotFound", "Search=Body", "Text=\"GUID\":{\"fieldtype\":\"static\",\"name\":\"GUID\",\"caption\":\"\{GUID\}\",\"value\":\"", LAST);
			
//		web_reg_save_param_regexp(
//		"ParamName=pkval2",
//		"RegExp=WA_PK}\",\"value\":\"(.*?)\",\"text(.*?)EstimateStatusValue}\",\"value\":\"9\"",
//		"Group=1",
//		"Ordinal=all",
//		SEARCH_FILTERS,
//		LAST);

			web_custom_request("ClaimsList",
				"URL={ADXE_URL}/api/Page/ClaimsList", 
				"Method=POST", 
				"Resource=0", 
				"RecContentType=application/json", 
				"Referer={ADXE_URL}/Estimating/WorkList", 
				"Snapshot=t12.inf", 
				"Mode=HTML", 
				"EncType=application/json;charset=UTF-8", 
				"Body={\"Form\":{\"ApptDateTime_Date\":\"\",\"ApptDateTime_Time\":\"\",\"Header_Filter\":\"\",\"Header_Filter_RO\":0,\"Header_Profiles\":\"All\",\"ItemsPerPage\":\"\",\"PageNo\":1,\"SortKey\":\"CreateDateTime_Date\"}}", 
				LAST);
			
	lr_end_transaction("RMS_AutoSource_T02_SortClaimList",LR_AUTO);	

	
	return 0;
}
