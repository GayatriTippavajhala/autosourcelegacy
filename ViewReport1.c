ViewReport1()
{
	
lr_start_transaction("RMS_AutoSource_T19_ViewReport");
	
	Lock();
		
//	lr_start_transaction("AS_Page_OpenExistingClaim");

	lr_start_sub_transaction("RMS_AutoSource_T19_ViewReport_ST00_Page_OpenExistingClaim", "RMS_AutoSource_T19_ViewReport");
	
		web_custom_request("OpenExitingClaim", 
			"URL={ADXE_URL}/api/Page/OpenExistingClaim?ClaimGUID={ClaimGUID}&ClmsElem=9&ItemType=R&LinkedItem=&ProfExternalID={Profile_Id}&ProfStampFlag=N&Status=9", 
			"Method=GET", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/WorkList", 
			"Snapshot=t47.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body=", 
			LAST);
//	lr_end_transaction("AS_Page_OpenExistingClaim", LR_AUTO);

	lr_end_sub_transaction("RMS_AutoSource_T19_ViewReport_ST00_Page_OpenExistingClaim", LR_AUTO);
	
	
//	lr_start_transaction("AS_Service_OpenClaim");
	
	lr_start_sub_transaction("RMS_AutoSource_T19_ViewReport_ST01_Service_OpenClaim", "RMS_AutoSource_T19_ViewReport");
	
		web_custom_request("OpenClaim", 
			"URL={ADXE_URL}/api/Service/OpenClaim", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/WorkList", 
			"Snapshot=t47.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"Action\":\"\",\"ClaimGUID\":\"{ClaimGUID}\",\"CopyClaim\":\"\",\"EstActiveFlag\":\"1\",\"EstimateStatus\":\"9\",\"HomeExternalID\":\"\"," 
			"\"ItemType\":\"R\",\"LinkedItem\":\"\",\"MergeGUID\":\"\",\"MergeProf\":\"\",\"NonSubscrMode\":\"N\",\"OfflineMode\":\"\",\"OpenClaimMain\":0,\"OpenClaimPrice\":\"\"," 
			"\"OpenContext\":\"\",\"OriginalLabor\":\"\",\"OriginalPrice\":\"\",\"PDCopy\":\"0\",\"ProfileId\":\"{Profile_Id}\",\"PxnResearchMethod\":\"\"," 
			"\"ReinspActiveFlag\":\"0\",\"ValActiveFlag\":\"1\",\"ValReport\":\"yes\",\"ViewDialog\":\"\"}}",
			LAST);
//	lr_end_transaction("AS_Service_OpenClaim", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T19_ViewReport_ST01_Service_OpenClaim", LR_AUTO);
	
//	lr_start_transaction("AS_Page_RptViewer");
	
	lr_start_sub_transaction("RMS_AutoSource_T19_ViewReport_ST02_Page_RptViewer", "RMS_AutoSource_T19_ViewReport");
	
		web_custom_request("RPTViewer", 
			"URL={ADXE_URL}/api/Page/RptViewer", 
			"Method=GET", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/ValuationReports", 
			"Snapshot=t47.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body=", 
			LAST);
//	lr_end_transaction("AS_Page_RptViewer", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T19_ViewReport_ST02_Page_RptViewer", LR_AUTO);
	
	
//	lr_start_transaction("AS_Page_Reports_Type");
	
	lr_start_sub_transaction("RMS_AutoSource_T19_ViewReport_ST03_Page_Reports_Type", "RMS_AutoSource_T19_ViewReport");
	
//			web_reg_save_param_ex("ParamName=Report_Type", "LB=\"OnClick\":\"OnShowReport(\'",	"RB=\')",
//				SEARCH_FILTERS,
//				"Scope=Body",
//				"IgnoreRedirections=No",
//				LAST);
	

	web_reg_save_param_regexp(
		"ParamName=Report_Type",
		"RegExp=OnClick\":\"OnShowReport\\('(.*?)'\\)\"",
		"Group=1",
		"Ordinal=1",
		SEARCH_FILTERS,
		LAST);
//	
//	web_reg_save_param_ex(
//		"ParamName=Type",
//		"LB=OnShowReport('",
//		"RB='",
//		SEARCH_FILTERS,
//		"Scope=Body",
//		"RequestUrl=*/Reports*",
//		LAST);

	
		web_custom_request("Reports", 
			"URL={ADXE_URL}/api/Page/Reports?Type=ASReport", 
			"Method=GET", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/ValuationReports", 
			"Snapshot=t47.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body=", 
			LAST);
//	lr_end_transaction("AS_Page_Reports_Type", LR_AUTO);
	
//	web_custom_request("OpenClaim", 
//		"URL=https://adxe-qa.apps-dev.gp2.axadmin.net/api/Service/OpenClaim", 
//		"Method=POST", 
//		"Resource=0", 
//		"RecContentType=text/plain", 
//		"Referer=https://adxe-qa.apps-dev.gp2.axadmin.net/Estimating/WorkList", 
//		"Snapshot=t4.inf", 
//		"Mode=HTML", 
//		"EncType=application/json;charset=UTF-8", 
//		"Body={\"Form\":{\"HandleJLP\":\"\",\"SuppNoticeGUID\":\"\",\"Action\":\"\",\"ClaimGUID\":\"{ClaimGUID}\",\"CopyClaim\":\"\",\"EstActiveFlag\":\"1\",\"EstimateStatus\":\"9\",\"HomeExternalID\":\"\",\"ItemType\":\"R\",\"LinkedItem\":\"\",\"MergeGUID\":\"\",\"MergeProf\":\"\",\"NonSubscrMode\":\"N\",\"OfflineMode\":\"\",\"OpenClaimPrice\":\"\",\"OpenContext\":\"\",\"OriginalLabor\":\"\",\"OriginalPrice\":\"\",\"PDCopy\":\"0\",\"ProfileId\":\"{Profile_Id}\",\"PxnResearchMethod\""
//		":\"\",\"ReinspActiveFlag\":\"0\",\"ValActiveFlag\":\"1\",\"ValReport\":\"yes\",\"ViewDialog\":\"\"}}", 
//		EXTRARES, 
//		"Url=/dist/b06871f281fee6b241d60582ae9369b9.ttf", "Referer=", ENDITEM, 
//		"Url=../Page/RptViewer", "Referer=https://adxe-qa.apps-dev.gp2.axadmin.net/Estimating/Autosource/ValuationReports", ENDITEM, 
//		"Url=https://app.pendo.io/data/guide.js/f3b3c9bc-7511-4425-5173-60fcdd5e11eb?jzb=eJx1j01r8zAQhP-K0fVNLEVuSPDNh36ktOWl6ccxbCPhCGStKq1MoOS_Z11K6KW33WcGZuZLjC47wrQxohUbekAwTzji9hAbtRAzAfs9lkCTHIr3M1GSZ-eBKOZWSjBHO_-EGmLMc2PHuo-6hiOYwYU6WJLXmdwA5EIvu0KYsaS9lW_gC0MMzzZiosxBgyUwQCDaS6fpdH_0MjSw8G5NdV98pdaV1m3TtItldfv48k8tG1VppRU7PX6wc3v3n29M_fdQ_m661dWkQugL9JahDbvXrThdNnM8PxGSDdT9Rlxz8qu1VCv5kzLalHkPY10v1_ViFxMacTqdATthcLQ&v=2.58.1_prod&ct=1594227810662", "Referer=https://adxe-qa.apps-dev.gp2.axadmin.net"
//		"/Estimating/Autosource/ValuationReports", ENDITEM, 
//		"Url=https://app.pendo.io/data/ptm.gif/f3b3c9bc-7511-4425-5173-60fcdd5e11eb?v=2.58.1_prod&ct=1594227810678&jzb="
//		"eJztVX9v4jgQ_SpR_saQBChb_msL6nLqttVVbbWqqsqJDbg4drAn_OiJ795JUiC9a1WoDna1KlKQx2PPezO2593948I84W7bjaSIRm7FDY2eWm4eQMQ47TcPG0HQ-uZ5Xt2vuBNhBWjzIBju6MGZpuxcT_TVMKl7Pm6mUaRTBblfpVJW3NRIXDoESGy7VqNsxsmYVmmSWML4pDpIgiqdURYLVVUcal2LuBSEGtRutRmdCQsYNjE6sW4buVIz4FCMBhj36vLoHP05nRPDKfAOft85ZdzgfCRxl4sDECAxm4ISBTBZtEXFjec9xfjMbXu4eCgkW5sJNVxBV_J4Ddfp3fwX7UTLNFYrNCYmBGgoOYm0dKLUWG1IXlxnihkRiSnlLirFQBHJ--A4m1BsvaaI5gcU36CEZ-uAQQ6KW1Js3wh76_K8g703sGF-BfaG5zAK9GWYnXJ-yIW534xJfqt2grnKa2rw-Zbe1xuOD9Gxl5TR0fwAvdQNCt"
//		"RIK6B4jw3py1Swddkzh9G4ZidFKFIkik626i9b57vb7rXb6NgaIyBYqK1g6huV6Pii8_NTWQT_Cr8o_3D_DGXlpas7WVvHmH1JBxjmzs2atHufL6NhL4MOO-Px-Pjy6S9WD9i4wbLVhsY8d96yE3s68kxr2njsj3SMTsutFVrlbtYdXIyaV0_e9VEHGvPsIoVz4IjkHwbNRWVTSW61Wo3fW5LXj1VSERMLFFLrlA0igMevZwxPtMkg9tI23xTpvI9wKUVixRNnJMLx2pEIKYupCZVL1p9RdDy9Mn80P6eqeZDi_wHMSuDXjBWfYnZY472pUajZ3BkKxomNsBnLkO5GFr6k-EuKv6T4D5LipRL_vVSBXyDCvl8SYYmC-q4G-95Bc88afJSCtjo1Ea_dUJnipFZFsbJntpLljPaSLFItVe3R1OvXcNrvc9rQlz-6_0_VgsODxf0ztlxQVg", "Referer=https://"
//		"adxe-qa.apps-dev.gp2.axadmin.net/Estimating/Autosource/ValuationReports", ENDITEM, 
//		"Url=../Page/Reports?Type=ASReport", "Referer=https://adxe-qa.apps-dev.gp2.axadmin.net/Estimating/Autosource/ValuationReports", ENDITEM, 
//		LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T19_ViewReport_ST03_Page_Reports_Type", LR_AUTO);
	
//	lr_start_transaction("AS_Service_GetReport_Type");
	
	lr_start_sub_transaction("RMS_AutoSource_T19_ViewReport_ST04_Service_GetReport_Type", "RMS_AutoSource_T19_ViewReport");
	
			web_reg_save_param_ex("ParamName=PDF_ID", "LB=", "RB=.pdf",
				SEARCH_FILTERS,
				"Scope=Body",
				"IgnoreRedirections=No",
				LAST);
		web_custom_request("GETReport", 
			"URL={ADXE_URL}/api/Service/GetReport?Type={Report_Type}", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/ValuationReports", 
			"Snapshot=t47.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{}}", 
			LAST);
//	lr_end_transaction("AS_Service_GetReport_Type", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T19_ViewReport_ST04_Service_GetReport_Type", LR_AUTO);
	
//	lr_start_transaction("AS_Page_Admin");
	
	lr_start_sub_transaction("RMS_AutoSource_T19_ViewReport_ST05_Page_Admin", "RMS_AutoSource_T19_ViewReport");
	
		web_url("Admin", 
			"URL={ADXE_URL}/api/Page/Admin", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Admin", 
			"Snapshot=t46.inf", 
			"Mode=HTML", 
			LAST);
//	lr_end_transaction("AS_Page_Admin", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T19_ViewReport_ST05_Page_Admin", LR_AUTO);
	
//	lr_start_transaction("AS_Session_GetFile");
	
	lr_start_sub_transaction("RMS_AutoSource_T19_ViewReport_ST06_Session_GetFile", "RMS_AutoSource_T19_ViewReport");

		web_reg_find("Search=Body",	"Text=PDF-1.",LAST);
		web_custom_request("GetFile", 
			"URL={ADXE_URL}/api/Session/Get?file={PDF_ID}.pdf", 
			"Method=GET", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/ValuationReports", 
			"Snapshot=t47.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body=", 
			LAST);
//	lr_end_transaction("AS_Session_GetFile", LR_AUTO);
		
		lr_end_sub_transaction("RMS_AutoSource_T19_ViewReport_ST06_Session_GetFile", LR_AUTO);

lr_end_transaction("RMS_AutoSource_T19_ViewReport", LR_AUTO);

	return 0;
}
