LogOut()
{
	lr_start_transaction("RMS_AutoSource_T20_LogOut");

//		lr_start_transaction("AS_LoginServices_Logout");

	lr_start_sub_transaction("RMS_AutoSource_T20_LogOut_ST00_LoginServices_Logout", "RMS_AutoSource_T20_LogOut");
	
			web_custom_request("SetupUpdate", 
				"URL={ADXE_URL}/api/LoginServices/Logout", 
				"Method=GET", 
				"Resource=0", 
				"RecContentType=text/plain", 
				"Referer={ADXE_URL}/Estimating/Worklist", 
				"Snapshot=t20.inf", 
				"Mode=HTML", 
				"EncType=application/json;charset=UTF-8", 
				"Body=", 
				LAST);
//		lr_end_transaction("AS_LoginServices_Logout", LR_AUTO);

	lr_end_sub_transaction("RMS_AutoSource_T20_LogOut_ST00_LoginServices_Logout", LR_AUTO);

	
//		lr_start_transaction("AS_Login");
	
	lr_start_sub_transaction("RMS_AutoSource_T20_LogOut_ST01_Login", "RMS_AutoSource_T20_LogOut");
	
			web_url("Login_2", 
				"URL={ADXE_URL}/Login", 
				"Resource=0", 
				"RecContentType=text/html", 
				"Referer={ADXE_URL}/Estimating/WorkList", 
				"Snapshot=t21.inf", 
				"Mode=HTML", 
				LAST);
//		lr_end_transaction("AS_Login", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T20_LogOut_ST01_Login", LR_AUTO);
	
//		lr_start_transaction("AS_CiecaSettings_CiecaEnabled");
//			web_url("CiecaEnabled_1", 
//				"URL={ADXE_URL}/api/CiecaSettings/CiecaEnabled", 
//				"Resource=0", 
//				"RecContentType=text/html", 
//				"Referer={ADXE_URL}/Estimating/Login", 
//				"Snapshot=t21.inf", 
//				"Mode=HTML", 
//				LAST);
//		lr_end_transaction("AS_CiecaSettings_CiecaEnabled", LR_AUTO);	
	
//		lr_start_transaction("AS_CiecaSettings_CiecaEnabled");
//			web_url("Config", 
//				"URL={ADXE_URL}/api/config", 
//				"Resource=0", 
//				"RecContentType=text/html", 
//				"Referer={ADXE_URL}/Login", 
//				"Snapshot=t21.inf", 
//				"Mode=HTML", 
//				LAST);
//		lr_end_transaction("AS_CiecaSettings_CiecaEnabled", LR_AUTO);	
		
	lr_end_transaction("RMS_AutoSource_T20_LogOut",LR_AUTO);
	
	return 0;
}
