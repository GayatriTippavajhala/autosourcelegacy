Lock()

	{
	lr_start_transaction("RMS_AutoSource_T05a_LockFile");
//	lr_start_sub_transaction("RMS_AutoSource_T05a_Lock_ST00_Service_LockFile", "RMS_AutoSource_T05a_Lock");
	
		web_custom_request("LockFile", 
			"URL={ADXE_URL}/api/Service/LockFile", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Admin", 
			"Snapshot=t47.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"Request\":{\"@Method\":\"LockFile\",\"GUID\":\"{ClaimGUID}\",\"fileType\":\"Claims\"}}}", 
			LAST);
	
//	lr_end_sub_transaction("RMS_AutoSource_T05a_LockUnlock_ST00_Service_LockFile", LR_AUTO);

	lr_end_transaction("RMS_AutoSource_T05a_LockFile", LR_AUTO);

	
	return 0;
	
}

UnLock()
{
	
lr_start_transaction("RMS_AutoSource_T05b_UnLockFile");
//	lr_start_sub_transaction("RMS_AutoSource_T05_SelectProduct_ST03_Service_UnlockFile", "RMS_AutoSource_T05_SelectProduct");
	
		web_custom_request("LockFile", 
			"URL={ADXE_URL}/api/Service/LockFile", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Admin", 
			"Snapshot=t47.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"Request\":{\"@Method\":\"UnlockFile\",\"GUID\":\"{ClaimGUID}\",\"fileType\":\"Claims\"}}}", 
			LAST);
	
//	lr_end_sub_transaction("RMS_AutoSource_T05_SelectProduct_ST03_Service_UnlockFile", LR_AUTO);

lr_end_transaction("RMS_AutoSource_T05b_UnLockFile", LR_AUTO);
	
	return 0;
	
}