WorkListColumns()
{
		lr_start_transaction("RMS_AutoSource_T17_ClickWorkListColumns");

	lr_start_sub_transaction("RMS_AutoSource_T17_ClickWorkListColumns_ST00_Page_UserPrefs", "RMS_AutoSource_T17_ClickWorkListColumns");

		
			web_custom_request("ClaimsList", 
				"URL={ADXE_URL}/api/Page/UserPrefs", 
				"Method=GET", 
				"Resource=0", 
				"RecContentType=application/json", 
				"Referer={ADXE_URL}/Estimating/WorkList", 
				"Snapshot=", 
				"Mode=HTML", 
				"EncType=application/json;charset=UTF-8", 
				"Body=",
				LAST);
//		lr_end_transaction("AS_Page_UserPrefs", LR_AUTO);	

	lr_end_sub_transaction("RMS_AutoSource_T17_ClickWorkListColumns_ST00_Page_UserPrefs", LR_AUTO);

		
//		lr_start_transaction("AS_Service_UserPrefsUpdate");	
	
	lr_start_sub_transaction("RMS_AutoSource_T17_ClickWorkListColumns_ST01_Service_UserPrefsUpdate", "RMS_AutoSource_T17_ClickWorkListColumns");
	
			web_custom_request("ClaimsList", 
				"URL={ADXE_URL}/api/Service/UserPrefsUpdate", 
				"Method=POST", 
				"Resource=0", 
				"RecContentType=application/json", 
				"Referer={ADXE_URL}/Estimating/WorkList", 
				"Snapshot=", 
				"Mode=HTML", 
				"EncType=application/json;charset=UTF-8", 
				"Body={\"Form\":{\"ClaimColumns\":[\"ClaimNo\",\"VehicleDescription1\",\"EstimateStatus\",\"MarketValue\",\"Images\",\"CreateDateTime_Date\",\"ValuationStatus\",\"VIN\"],\"PageName\":\"UserPrefs\",\"SaveData\":\"1\"}}",
				LAST);
		
	lr_end_sub_transaction("RMS_AutoSource_T17_ClickWorkListColumns_ST01_Service_UserPrefsUpdate", LR_AUTO);

lr_end_transaction("RMS_AutoSource_T17_ClickWorkListColumns", LR_AUTO);

return 0;
}
