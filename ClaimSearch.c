ClaimSearch()
{

	lr_start_transaction("RMS_AutoSource_T18_ClaimSearch");	
	

	web_reg_find("SaveCount=ReportCheck",
		"Text=caption\":\"Claim # :\",\"value\":\"{ClaimNum_Cap}\"",
		LAST);

	web_custom_request("ClaimsList", 
		"URL={ADXE_URL}/api/Page/ClaimSearch", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer={ADXE_URL}/Estimating/WorkList", 
		"Snapshot=", 
		"Mode=HTML", 
		"EncType=application/json;charset=UTF-8", 
		"Body={\"Form\":{\"ApptDateTime_Date\":\"\",\"ApptDateTime_Time\":\"00:00 AM\",\"ClaimNo\":\"{ClaimNum_Cap}\",\"ClaimNo2\":\"\"," 
		"\"DriveInAssnRetrivalFlg\":\"1\",\"FileNo\":\"\",\"Header_Profiles\":\"All\",\"InsCoList\":\"\",\"InsCoSe\":\"0\",\"Insured\":\"\",\"LossDate\":\"  /  /    \"," 
		"\"LossDate_Time\":\"00:00 AM\",\"Owner\":\"\",\"Owner2\":\"\",\"PageNo\":\"1\",\"PolicyNo\":\"\",\"PolicyNo2\":\"\",\"" 
		"RONumber\":\"\",\"SearchType\":\"Search\",\"SortKey\":\"\",\"SrchEnabled\":\"1\",\"SrchResult\":\"0\",\"SrchSelection\":\"0\",\"VIN\":\"\",\"VIN2\":\"\"}}",
		LAST);
	
	if (atoi(lr_eval_string("{ReportCheck}")) < 1)
	    {
    		lr_error_message("Report not found for Claim Number: %s, Can not continue to View Report transaction. Exiting the iteration",  lr_eval_string("{ClaimNum_Cap}"));
		lr_exit(LR_EXIT_MAIN_ITERATION_AND_CONTINUE, LR_FAIL);
	    	
	    	return 0;
	    }
	
	
	lr_end_transaction("RMS_AutoSource_T18_ClaimSearch",LR_AUTO);	
	
	
	
	return 0;
}
