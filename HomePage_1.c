HomePage_1()
{

	
lr_start_transaction("RMS_AutoSource_T00_HomePage");
	
	web_add_header("Upgrade-Insecure-Requests", "1");

	web_url("adxe-qa.apps-dev.gp2.axadmin.net", 
		"URL={ADXE_URL}/", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t1.inf", 
		"Mode=HTML", 
		LAST);


	web_url("CiecaEnabled", 
		"URL={ADXE_URL}/api/CiecaSettings/CiecaEnabled", 
		"Resource=0", 
		"RecContentType=text/plain", 
		"Referer=ADXE_URL/Login", 
		"Snapshot=t28.inf", 
		"Mode=HTML", 
		LAST);

	web_reg_save_param_ex("ParamName=cieca_acl_URL", "LB=\"ciecaAclUrl\": \"",	"RB=\"",
		SEARCH_FILTERS,
		"Scope=Body",
		"IgnoreRedirections=No",
		LAST);	
				
	web_url("config", 
		"URL={ADXE_URL}/api/config", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=ADXE_URL/Login", 
		"Snapshot=t29.inf", 
		"Mode=HTML", 
		LAST);
		
lr_end_transaction("RMS_AutoSource_T00_HomePage",LR_AUTO);
	return 0;
}
