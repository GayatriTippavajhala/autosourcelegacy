Cieca_import_export()
{
	lr_continue_on_error (1);
	
		lr_start_transaction("AS_Cieca_ImportSettings");
		
			web_reg_find("Search=Body", "Text=SetupBSMS",	LAST);
			
			web_url("ImportSettings", 
				"URL={cieca_acl_URL}/api/v1/ImportSettings?ticket={Cookie_1}&applicationType=ADXE", 
				"Resource=0", 
				"RecContentType=application/json", 
				"Referer={ADXE_URL}/Estimating/WorkList", 
				"Snapshot=t19.inf", 
				"Mode=HTML", 
				LAST);
			
		lr_end_transaction("AS_Cieca_ImportSettings", LR_AUTO);
		
	lr_continue_on_error (0);
	
	return 0;		
}

Cieca_Export()
{
	lr_continue_on_error (1);
	
		lr_start_transaction("AS_Cieca_ExportSettings");
		
			web_reg_find("Search=Body", "Text=SetupBSMS",	LAST);
			
			web_add_header("Origin", "{ADXE_URL}");
			
			web_url("ExportSettings", 
				"URL={cieca_acl_URL}/api/v1/ExportSettings?ticket={Cookie_1}&applicationType=ADXE", 
				"Resource=0", 
				"RecContentType=application/json", 
				"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
				"Snapshot=t19.inf", 
				"Mode=HTML", 
				LAST);
			
		lr_end_transaction("AS_Cieca_ExportSettings", LR_AUTO);
		
	lr_continue_on_error (0);
		
	return 0;		
}
	
