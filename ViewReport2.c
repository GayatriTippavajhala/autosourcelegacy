ViewReport2()
{
	lr_start_transaction("RMS_AutoSource_T19_ViewReport");
	

	web_reg_find("SaveCount=CompletedClaims",
		"Text=EstimateStatusValue}\",\"value\":\"9\"",
		LAST);

	web_custom_request("ClaimsList_3",
		"URL=https://adxe-qa.apps-dev.gp2.axadmin.net/api/Page/ClaimsList", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://adxe-qa.apps-dev.gp2.axadmin.net/Estimating/WorkList", 
		"Snapshot=t42.inf", 
		"Mode=HTTP", 
		"EncType=application/json;charset=UTF-8", 
		"Body={\"Form\":{\"ApptDateTime_Date\":\"\",\"ApptDateTime_Time\":\"\",\"Header_Filter\":\"\",\"Header_Filter_RO\":0,\"Header_Profiles\":\"All\",\"ItemsPerPage\":\"\",\"PageNo\":1,\"SortKey\":\"CreateDateTime_Date\"}}", 
//		"Body={\"Form\":{\"ApptDateTime_Date\":\"\",\"ApptDateTime_Time\":\"\",\"Header_Filter\":\"\",\"Header_Filter_RO\":0,\"Header_Profiles\":\"All\",\"ItemsPerPage\":\"\",\"PageNo\":1,\"SortKey\":\"\"}}", 
		LAST);
		
		
if(atoi(lr_eval_string("{CompletedClaims}")) < 1)
{
		
	web_custom_request("ClaimsList_3",
		"URL=https://adxe-qa.apps-dev.gp2.axadmin.net/api/Page/ClaimsList", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://adxe-qa.apps-dev.gp2.axadmin.net/Estimating/WorkList", 
		"Snapshot=t42.inf", 
		"Mode=HTTP", 
		"EncType=application/json;charset=UTF-8", 
		"Body={\"Form\":{\"ApptDateTime_Date\":\"\",\"ApptDateTime_Time\":\"\",\"Header_Filter\":\"\",\"Header_Filter_RO\":0,\"Header_Profiles\":\"All\",\"ItemsPerPage\":\"\",\"PageNo\":1,\"SortKey\":\"CreateDateTime_Date\"}}", 
//		"Body={\"Form\":{\"ApptDateTime_Date\":\"\",\"ApptDateTime_Time\":\"\",\"Header_Filter\":\"\",\"Header_Filter_RO\":0,\"Header_Profiles\":\"All\",\"ItemsPerPage\":\"\",\"PageNo\":1,\"SortKey\":\"\"}}", 
//		"Body={\"Form\":{\"ApptDateTime_Date\":\"\",\"ApptDateTime_Time\":\"\",\"Header_Filter\":\"\",\"Header_Filter_RO\":0,\"Header_Profiles\":\"All\",\"ItemsPerPage\":\"\",\"PageNo\":1,\"SortKey\":\"EstimateStatus\"}}",
		LAST);
}

		lr_start_sub_transaction("RMS_AutoSource_T19_ViewReport_ST00_Page_ClaimsList", "RMS_AutoSource_T19_ViewReport");

//text":"Supp Notice"}],"value":"Report(.*?)WA_PK}","value":"(.*?)","text(.*?)EstimateStatusValue}","value":"9","text

web_reg_save_param_regexp(
		"ParamName=pkval",
		"RegExp=text\":\"Supp Notice\"}\],\"value\":\"Report(.*?)WA_PK}\",\"value\":\"(.*?)\",\"text(.*?)EstimateStatusValue}\",\"value\":\"9\",\"text",
		"Group=2",
		"Ordinal=1",
		SEARCH_FILTERS,
		LAST);
		
	
	web_custom_request("ClaimsList_3",
		"URL=https://adxe-qa.apps-dev.gp2.axadmin.net/api/Page/ClaimsList", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=application/json", 
		"Referer=https://adxe-qa.apps-dev.gp2.axadmin.net/Estimating/WorkList", 
		"Snapshot=t42.inf", 
		"Mode=HTTP", 
		"EncType=application/json;charset=UTF-8", 
//		"Body={\"Form\":{\"ApptDateTime_Date\":\"\",\"ApptDateTime_Time\":\"\",\"Header_Filter\":\"\",\"Header_Filter_RO\":0,\"Header_Profiles\":\"All\",\"ItemsPerPage\":\"\",\"PageNo\":1,\"SortKey\":\"CreateDateTime_Date\"}}", 
//		"Body={\"Form\":{\"ApptDateTime_Date\":\"\",\"ApptDateTime_Time\":\"\",\"Header_Filter\":\"\",\"Header_Filter_RO\":0,\"Header_Profiles\":\"All\",\"ItemsPerPage\":\"\",\"PageNo\":1,\"SortKey\":\"EstimateStatus\"}}",
		"Body={\"Form\":{\"ApptDateTime_Date\":\"\",\"ApptDateTime_Time\":\"\",\"Header_Filter\":\"\",\"Header_Filter_RO\":0,\"Header_Profiles\":\"All\",\"ItemsPerPage\":\"\",\"PageNo\":1,\"SortKey\":\"\"}}", 
		LAST);

	lr_end_sub_transaction("RMS_AutoSource_T19_ViewReport_ST00_Page_ClaimsList", LR_AUTO);

/*	web_add_auto_header("Sec-Fetch-Site", 
		"cross-site");

	web_custom_request("NRJS-d6de7d4fe42a9abcb66_9", 
		"URL=https://bam.nr-data.net/resources/1/NRJS-d6de7d4fe42a9abcb66?a=692400069&sa=1&v=1167.2a4546b&t=Unnamed%20Transaction&rst=52835&ref=https://adxe-qa.apps-dev.gp2.axadmin.net/Estimating/WorkList&st=1594286939438&ptid=4864fd34-001f-b541-ea21-017332e82676", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=text/plain", 
		"Referer=https://adxe-qa.apps-dev.gp2.axadmin.net/Estimating/WorkList", 
		"Snapshot=t43.inf", 
		"Mode=HTTP", 
		"EncType=text/plain", 
		"Body={\"res\":[{\"n\":\"readystatechange\",\"s\":41866,\"e\":41866,\"t\":\"event\",\"o\":\"undefined POST: bam.nr-data.net:443/resources/1/NRJS-d6de7d4fe42a9abcb66\"},{\"n\":\"readystatechange\",\"s\":41870,\"e\":41870,\"t\":\"event\",\"o\":\"undefined GET: adxe-qa.apps-dev.gp2.axadmin.net:443/api/LoginServices/GetUserInfo\"},{\"n\":\"readystatechange\",\"s\":41870,\"e\":41870,\"t\":\"event\",\"o\":\"undefined GET: adxe-qa.apps-dev.gp2.axadmin.net:443/api/LoginServices/GetUserInfo\"},{\"n\":\""
		"readystatechange\",\"s\":41871,\"e\":41871,\"t\":\"event\",\"o\":\"undefined GET: adxe-qa.apps-dev.gp2.axadmin.net:443/api/LoginServices/GetUserInfo\"},{\"n\":\"readystatechange\",\"s\":41871,\"e\":41871,\"t\":\"event\",\"o\":\"undefined GET: adxe-qa.apps-dev.gp2.axadmin.net:443/api/LoginServices/GetUserInfo\"},{\"n\":\"readystatechange\",\"s\":41871,\"e\":41871,\"t\":\"event\",\"o\":\"undefined GET: adxe-qa.apps-dev.gp2.axadmin.net:443/api/LoginServices/GetUserInfo\"},{\"n\":\"readystatechange\","
		"\"s\":41872,\"e\":41872,\"t\":\"event\",\"o\":\"undefined GET: adxe-qa.apps-dev.gp2.axadmin.net:443/api/LoginServices/GetUserInfo\"},{\"n\":\"readystatechange\",\"s\":41908,\"e\":41908,\"t\":\"event\",\"o\":\"undefined POST: bam.nr-data.net:443/resources/1/NRJS-d6de7d4fe42a9abcb66\"},{\"n\":\"readystatechange\",\"s\":41908,\"e\":41908,\"t\":\"event\",\"o\":\"undefined POST: bam.nr-data.net:443/resources/1/NRJS-d6de7d4fe42a9abcb66\"},{\"n\":\"readystatechange\",\"s\":43682,\"e\":43682,\"t\":\""
		"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"readystatechange\",\"s\":43682,\"e\":43682,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"readystatechange\",\"s\":43683,\"e\":43683,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"readystatechange\",\"s\":43683,\"e\":43683,\"t\":\"event\",\"o\":\"undefined POST: "
		"adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"readystatechange\",\"s\":43692,\"e\":43692,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"readystatechange\",\"s\":43692,\"e\":43692,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"readystatechange\",\"s\":43798,\"e\":43798,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/"
		"Service/ProfileSvr\"},{\"n\":\"readystatechange\",\"s\":43802,\"e\":43802,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\"},{\"n\":\"readystatechange\",\"s\":44421,\"e\":44421,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"readystatechange\",\"s\":44450,\"e\":44450,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\"},{\"n\":\""
		"readystatechange\",\"s\":44450,\"e\":44450,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\"},{\"n\":\"readystatechange\",\"s\":44450,\"e\":44450,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\"},{\"n\":\"readystatechange\",\"s\":44450,\"e\":44450,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\"},{\"n\":\"readystatechange\",\"s\":44451,\"e\""
		":44451,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\"},{\"n\":\"readystatechange\",\"s\":44451,\"e\":44451,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\"},{\"n\":\"readystatechange\",\"s\":44490,\"e\":44490,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\"},{\"n\":\"readystatechange\",\"s\":44490,\"e\":44490,\"t\":\"event\",\"o\":\""
		"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\"},{\"n\":\"readystatechange\",\"s\":44490,\"e\":44490,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\"},{\"n\":\"readystatechange\",\"s\":44490,\"e\":44490,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\"},{\"n\":\"readystatechange\",\"s\":44491,\"e\":44491,\"t\":\"event\",\"o\":\"undefined POST: "
		"adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\"},{\"n\":\"readystatechange\",\"s\":44491,\"e\":44491,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\"},{\"n\":\"readystatechange\",\"s\":45682,\"e\":45682,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"readystatechange\",\"s\":45682,\"e\":45682,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api"
		"/Page/ClaimsList\"},{\"n\":\"readystatechange\",\"s\":45682,\"e\":45682,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"readystatechange\",\"s\":45682,\"e\":45682,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"readystatechange\",\"s\":45700,\"e\":45701,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"readystatechange"
		"\",\"s\":45701,\"e\":45701,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"readystatechange\",\"s\":50170,\"e\":50170,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"readystatechange\",\"s\":51806,\"e\":51807,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"readystatechange\",\"s\":51807,\"e\":51807,\"t\":\"event\",\""
		"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"readystatechange\",\"s\":51807,\"e\":51807,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"readystatechange\",\"s\":51807,\"e\":51807,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"readystatechange\",\"s\":51818,\"e\":51818,\"t\":\"event\",\"o\":\"undefined POST: "
		"adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"readystatechange\",\"s\":51818,\"e\":51819,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"setTimeout\",\"s\":41853,\"e\":41869,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":41966,\"e\":41966,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":42259,\"e\":42259,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":42760,\"e\":42761,\"o"
		"\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":43261,\"e\":43262,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":43566,\"e\":43566,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44469,\"e\":44473,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44474,\"e\":44474,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44474,\"e\":44474,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44474,\"e\":44478,\"o\":\"window\",\"t\":\""
		"timer\"},{\"n\":\"setTimeout\",\"s\":44478,\"e\":44478,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44478,\"e\":44479,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44480,\"e\":44480,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44490,\"e\":44490,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44601,\"e\":44601,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44601,\"e\":44601,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\""
		"setTimeout\",\"s\":44601,\"e\":44603,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44604,\"e\":44605,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44606,\"e\":44606,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44606,\"e\":44606,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44606,\"e\":44606,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44606,\"e\":44606,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\""
		":44606,\"e\":44606,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44608,\"e\":44608,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44608,\"e\":44608,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44608,\"e\":44608,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44629,\"e\":44629,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44631,\"e\":44631,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44710,\"e\":44711,\""
		"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44720,\"e\":44720,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44722,\"e\":44722,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44777,\"e\":44777,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44778,\"e\":44778,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44779,\"e\":44779,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44779,\"e\":44779,\"o\":\"window\",\"t\":"
		"\"timer\"},{\"n\":\"setTimeout\",\"s\":44781,\"e\":44781,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44781,\"e\":44782,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44782,\"e\":44782,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44784,\"e\":44784,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44785,\"e\":44785,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44785,\"e\":44785,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\""
		"setTimeout\",\"s\":44787,\"e\":44787,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44787,\"e\":44787,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44791,\"e\":44792,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44792,\"e\":44792,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44796,\"e\":44796,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44796,\"e\":44797,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\""
		":44797,\"e\":44797,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44797,\"e\":44797,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44797,\"e\":44797,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44797,\"e\":44797,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44801,\"e\":44801,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44801,\"e\":44802,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44802,\"e\":44802,\""
		"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44802,\"e\":44802,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44802,\"e\":44802,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44802,\"e\":44802,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44806,\"e\":44807,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44807,\"e\":44807,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44807,\"e\":44807,\"o\":\"window\",\"t\":"
		"\"timer\"},{\"n\":\"setTimeout\",\"s\":44807,\"e\":44807,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44811,\"e\":44812,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44812,\"e\":44812,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44812,\"e\":44812,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44812,\"e\":44812,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44817,\"e\":44818,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\""
		"setTimeout\",\"s\":44818,\"e\":44818,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44818,\"e\":44818,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44818,\"e\":44818,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44823,\"e\":44823,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44823,\"e\":44824,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44824,\"e\":44824,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\""
		":44824,\"e\":44824,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44828,\"e\":44828,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44828,\"e\":44828,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44828,\"e\":44829,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44829,\"e\":44829,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44834,\"e\":44834,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44834,\"e\":44834,\""
		"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44838,\"e\":44839,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44839,\"e\":44839,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44843,\"e\":44843,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44843,\"e\":44844,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44848,\"e\":44849,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":44853,\"e\":44853,\"o\":\"window\",\"t\":"
		"\"timer\"},{\"n\":\"setTimeout\",\"s\":44858,\"e\":44864,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":45094,\"e\":45094,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":45099,\"e\":45099,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":45369,\"e\":45370,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":46972,\"e\":46973,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":46973,\"e\":46973,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\""
		"setTimeout\",\"s\":46973,\"e\":46973,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":46973,\"e\":46973,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":47474,\"e\":47475,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":47978,\"e\":47979,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":48480,\"e\":48481,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":48985,\"e\":48986,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\""
		":49488,\"e\":49489,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":49993,\"e\":49994,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":50182,\"e\":50182,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":50495,\"e\":50497,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":50998,\"e\":50999,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"setTimeout\",\"s\":51500,\"e\":51501,\"o\":\"window\",\"t\":\"timer\"},{\"n\":\"progress\",\"s\":41871,\"e\":41871,\"t\""
		":\"event\",\"o\":\"undefined GET: adxe-qa.apps-dev.gp2.axadmin.net:443/api/LoginServices/GetUserInfo\"},{\"n\":\"progress\",\"s\":43683,\"e\":43683,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"progress\",\"s\":43692,\"e\":43692,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"progress\",\"s\":44450,\"e\":44450,\"t\":\"event\",\"o\":\"undefined POST: "
		"adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\"},{\"n\":\"progress\",\"s\":44491,\"e\":44491,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\"},{\"n\":\"progress\",\"s\":45682,\"e\":45682,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"progress\",\"s\":45700,\"e\":45700,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\""
		"n\":\"progress\",\"s\":51807,\"e\":51807,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"progress\",\"s\":51818,\"e\":51818,\"t\":\"event\",\"o\":\"undefined POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"load\",\"s\":41897,\"e\":41897,\"t\":\"event\",\"o\":\"200 GET: adxe-qa.apps-dev.gp2.axadmin.net:443/api/LoginServices/GetUserInfo\"},{\"n\":\"load\",\"s\":41897,\"e\":41898,\"t\":\"event\",\"o\":\"200 GET: "
		"adxe-qa.apps-dev.gp2.axadmin.net:443/api/LoginServices/GetUserInfo\"},{\"n\":\"load\",\"s\":41908,\"e\":41908,\"t\":\"event\",\"o\":\"200 POST: bam.nr-data.net:443/resources/1/NRJS-d6de7d4fe42a9abcb66\"},{\"n\":\"load\",\"s\":41909,\"e\":41909,\"t\":\"event\",\"o\":\"200 POST: bam.nr-data.net:443/resources/1/NRJS-d6de7d4fe42a9abcb66\"},{\"n\":\"load\",\"s\":44423,\"e\":44423,\"t\":\"event\",\"o\":\"200 POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"load\",\"s\":44423,\""
		"e\":44424,\"t\":\"event\",\"o\":\"200 POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"load\",\"s\":44451,\"e\":44451,\"t\":\"event\",\"o\":\"200 POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\"},{\"n\":\"load\",\"s\":44452,\"e\":44452,\"t\":\"event\",\"o\":\"200 POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\"},{\"n\":\"load\",\"s\":44493,\"e\":44494,\"t\":\"event\",\"o\":\"200 POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/"
		"ProfileSvr\"},{\"n\":\"load\",\"s\":44494,\"e\":44494,\"t\":\"event\",\"o\":\"200 POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\"},{\"n\":\"load\",\"s\":46948,\"e\":46948,\"t\":\"event\",\"o\":\"200 POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"load\",\"s\":46948,\"e\":46948,\"t\":\"event\",\"o\":\"200 POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"load\",\"s\":52811,\"e\":52811,\"t\":\"event\",\"o\":\"200 POST: "
		"adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"load\",\"s\":52811,\"e\":52811,\"t\":\"event\",\"o\":\"200 POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\"},{\"n\":\"Ajax\",\"s\":41572,\"e\":41898,\"o\":\"200 GET: adxe-qa.apps-dev.gp2.axadmin.net:443/api/LoginServices/GetUserInfo\",\"t\":\"ajax\"},{\"n\":\"Ajax\",\"s\":41867,\"e\":41909,\"o\":\"200 POST: bam.nr-data.net:443/resources/1/NRJS-d6de7d4fe42a9abcb66\",\"t\":\"ajax\"},{\"n\":\"Ajax\",\"s\":41576,\"e\""
		":44424,\"o\":\"200 POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\",\"t\":\"ajax\"},{\"n\":\"Ajax\",\"s\":43799,\"e\":44452,\"o\":\"200 POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\",\"t\":\"ajax\"},{\"n\":\"Ajax\",\"s\":43802,\"e\":44494,\"o\":\"200 POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\",\"t\":\"ajax\"},{\"n\":\"Ajax\",\"s\":44421,\"e\":46948,\"o\":\"200 POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\",\"t\":\"ajax"
		"\"},{\"n\":\"Ajax\",\"s\":50171,\"e\":52811,\"o\":\"200 POST: adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\",\"t\":\"ajax\"},{\"n\":\"mousing\",\"s\":44485,\"e\":44485,\"t\":\"event\",\"o\":\"li#top-level-sidenav.sidebar-heading.sidebar-menu.cursor-click.navparent\"},{\"n\":\"mousing\",\"s\":46954,\"e\":46954,\"t\":\"event\",\"o\":\"li#top-level-sidenav.sidebar-heading.sidebar-menu.cursor-click.navparent\"},{\"n\":\"mousing\",\"s\":46954,\"e\":46954,\"t\":\"event\",\"o\":\""
		"span#CreateDateHeader\"},{\"n\":\"mousing\",\"s\":50180,\"e\":50180,\"t\":\"event\",\"o\":\"span#CreateDateHeader\"},{\"n\":\"mousing\",\"s\":50180,\"e\":50180,\"t\":\"event\",\"o\":\"div.spinner-overlay\"},{\"n\":\"message\",\"s\":46971,\"e\":46971,\"t\":\"event\",\"o\":\"window\"},{\"n\":\"message\",\"s\":46971,\"e\":46971,\"t\":\"event\",\"o\":\"window\"},{\"n\":\"message\",\"s\":46971,\"e\":46971,\"t\":\"event\",\"o\":\"window\"},{\"n\":\"message\",\"s\":46971,\"e\":46971,\"t\":\"event\",\"o\""
		":\"window\"},{\"n\":\"message\",\"s\":46971,\"e\":46971,\"t\":\"event\",\"o\":\"window\"},{\"n\":\"message\",\"s\":46971,\"e\":46971,\"t\":\"event\",\"o\":\"window\"},{\"n\":\"message\",\"s\":46972,\"e\":46972,\"t\":\"event\",\"o\":\"window\"},{\"n\":\"message\",\"s\":46972,\"e\":46972,\"t\":\"event\",\"o\":\"window\"},{\"n\":\"selectionchange\",\"s\":50056,\"e\":50056,\"t\":\"event\",\"o\":\"document\"},{\"n\":\"click\",\"s\":50133,\"e\":50135,\"t\":\"event\",\"o\":\"span#CreateDateHeader\"},{\""
		"n\":\"click\",\"s\":50144,\"e\":50144,\"t\":\"event\",\"o\":\"span#CreateDateHeader\"},{\"n\":\"click\",\"s\":50144,\"e\":50147,\"t\":\"event\",\"o\":\"span#CreateDateHeader\"},{\"n\":\"click\",\"s\":50147,\"e\":50164,\"t\":\"event\",\"o\":\"span#CreateDateHeader\"},{\"n\":\"img\",\"s\":41688,\"e\":41775,\"o\":\"https://app.pendo.io:443/data/ptm.gif/f3b3c9bc-7511-4425-5173-60fcdd5e11eb\",\"t\":\"resource\"},{\"n\":\"img\",\"s\":44473,\"e\":44564,\"o\":\"https://app.pendo.io:443/data/ptm.gif/"
		"f3b3c9bc-7511-4425-5173-60fcdd5e11eb\",\"t\":\"resource\"},{\"n\":\"xmlhttprequest\",\"s\":41868,\"e\":41906,\"o\":\"https://bam.nr-data.net:443/resources/1/NRJS-d6de7d4fe42a9abcb66\",\"t\":\"resource\"},{\"n\":\"xmlhttprequest\",\"s\":43800,\"e\":44206,\"o\":\"https://adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\",\"t\":\"resource\"},{\"n\":\"xmlhttprequest\",\"s\":43805,\"e\":44205,\"o\":\"https://adxe-qa.apps-dev.gp2.axadmin.net:443/api/Service/ProfileSvr\",\"t\":\"resource\"},{"
		"\"n\":\"xmlhttprequest\",\"s\":44422,\"e\":45698,\"o\":\"https://adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\",\"t\":\"resource\"},{\"n\":\"xmlhttprequest\",\"s\":50173,\"e\":51816,\"o\":\"https://adxe-qa.apps-dev.gp2.axadmin.net:443/api/Page/ClaimsList\",\"t\":\"resource\"},{\"n\":\"script\",\"s\":43823,\"e\":44066,\"o\":\"https://app.pendo.io:443/data/guide.js/f3b3c9bc-7511-4425-5173-60fcdd5e11eb\",\"t\":\"resource\"},{\"n\":\"script\",\"s\":44477,\"e\":44582,\"o\":\"https://"
		"app.pendo.io:443/data/guide.js/f3b3c9bc-7511-4425-5173-60fcdd5e11eb\",\"t\":\"resource\"},{\"n\":\"script\",\"s\":44602,\"e\":44759,\"o\":\"https://pendo-static-5764269309427712.storage.googleapis.com:443/guide-content/JYpR_ZrUCMZ9QXRohHWWqLU6dVs/nZZCqGUTeqHN7dyaUPOrz-AxMsI/Kj3_DDkMlaU3D5pCJeXB7sK8rmQ.dom.jsonp\",\"t\":\"resource\"},{\"n\":\"script\",\"s\":44604,\"e\":44758,\"o\":\"https://pendo-static-5764269309427712.storage.googleapis.com:443/guide-content/fTBngJfWa6KvZsl0mGqtNhTy8-M/"
		"0eREeo_JR9SpK-sQRHOAnoJr700/k8Sl1VRCSr61qAlEzTRO6BptWfk.dom.jsonp\",\"t\":\"resource\"},{\"n\":\"link\",\"s\":44591,\"e\":44620,\"o\":\"https://cdn.pendo.io:443/agent/releases/2.58.1/guide.css\",\"t\":\"resource\"},{\"n\":\"link\",\"s\":44593,\"e\":44756,\"o\":\"https://pendo-static-5764269309427712.storage.googleapis.com:443/guide.-323232.1589488039377.css\",\"t\":\"resource\"}]}", 
		LAST);
*/
	web_add_cookie("ticket=ADPFalcon=FalconTicket={platFormToken}&ADXEMode=1&USENGLISH=USENGLISH&SpecialtySet=no&SingleValuation=0&ADPcompanycode=&ViewerPermCall=&ADPListMode=&ADPUSRExternalID={ProfileID_2}&MoveWorkSharingFlg=0&GlobalRatesFlag=0&ADPReadOnlyValControls=0&ADPClaimGuid=&CallPQEQuoteId=0&ADPContext=ClaimsList&ADPOrder=D&ADPExternalID={ProfileID_2}&ADPUserID={UserID}&ADPTz=0&NonSubscr=&ADPcountrycode=Us&ADPWorklistFilterRO=0&ADPWorklistFilter=&ADPProfile=All&"
		"ConvertDlg=1&PreUpdMode=0&PreUpdReqNbr=&PreUpdRepNbr=&PreUpdReinsp=&ADPUpdateDialog=0&TLMAuthToken=&Pentools=0&ClaimsListPageNo=1&ADPSHPExternalID={ProfileID_3}&ADPLdapUIDName=&ADPLdapUserOrgName={ProfileID_3}&TLMPosted=&UpdValOrigProdInfo=&PreUpdExtID=&ADPAllMode=1&ADPEstimateEnabled=1&ADPLdapSessionTimeOut=3600&ShowWhatsNew=1&ADPValVehNeeded=0&ADPCatViewEnabled=0&AutoImport=0&ADPColumn=CreateDateTime%255FDate&ADPlob=SHP&ClientAppVersion=10%252E03%252E440&GlobalVehicle=0%253B; DOMAIN="
		"adxe-qa.apps-dev.gp2.axadmin.net");

	web_add_auto_header("Sec-Fetch-Site", 
		"same-origin");
	
	lr_start_sub_transaction("RMS_AutoSource_T19_ViewReport_ST01_Service_LockFile", "RMS_AutoSource_T19_ViewReport");
	
		//GUID":"FD8E07B5-99D7-4A67-B79C-53481162792B"
//		  GUID":"52130D2E-240D-4B41-9982-657923B14837"
	web_reg_save_param_ex(
		"ParamName=ReportGUID",
		"LB=GUID\":\"",
		"RB=\"",
		"Ordinal=1",
		SEARCH_FILTERS,
		LAST);
	

	web_custom_request("LockFile", 
		"URL=https://adxe-qa.apps-dev.gp2.axadmin.net/api/Service/LockFile", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=text/plain", 
		"Referer=https://adxe-qa.apps-dev.gp2.axadmin.net/Estimating/WorkList", 
		"Snapshot=t44.inf", 
		"Mode=HTTP", 
		"EncType=application/json;charset=UTF-8", 
		"Body={\"Form\":{\"Request\":{\"@Method\":\"LockFile\",\"GUID\":\"pk-{pkval}\",\"fileType\":\"Claims\",\"Prf\":\"{ProfileID_2}\"}}}", 
		LAST);
		
		lr_end_sub_transaction("RMS_AutoSource_T19_ViewReport_ST01_Service_LockFile", LR_AUTO);

	web_add_cookie("EstimateItemType=R; DOMAIN=adxe-qa.apps-dev.gp2.axadmin.net");
	
	lr_start_sub_transaction("RMS_AutoSource_T19_ViewReport_ST02_Page_OpenExistingClaim", "RMS_AutoSource_T19_ViewReport");

	web_custom_request("OpenExistingClaim", 
		"URL=https://adxe-qa.apps-dev.gp2.axadmin.net/api/Page/OpenExistingClaim?ClaimGUID={ReportGUID}&ClmsElem=9&ItemType=R&LinkedItem=&ProfExternalID={ProfileID_2}&ProfStampFlag=Y&Status=9", 
		"Method=GET", 
		"Resource=1", 
		"RecContentType=text/plain", 
		"Referer=https://adxe-qa.apps-dev.gp2.axadmin.net/Estimating/WorkList", 
		"Snapshot=t45.inf", 
		LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T19_ViewReport_ST02_Page_OpenExistingClaim", LR_AUTO);
	
	lr_start_sub_transaction("RMS_AutoSource_T19_ViewReport_ST03_Service_OpenClaim", "RMS_AutoSource_T19_ViewReport");

	web_custom_request("OpenClaim", 
		"URL=https://adxe-qa.apps-dev.gp2.axadmin.net/api/Service/OpenClaim", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=text/plain", 
		"Referer=https://adxe-qa.apps-dev.gp2.axadmin.net/Estimating/WorkList", 
		"Snapshot=t46.inf", 
		"Mode=HTTP", 
		"EncType=application/json;charset=UTF-8", 
		"Body={\"Form\":{\"HandleJLP\":\"\",\"SuppNoticeGUID\":\"\",\"Action\":\"\",\"ClaimGUID\":\"{ReportGUID}\",\"CopyClaim\":\"\",\"EstActiveFlag\":\"1\",\"EstimateStatus\":\"9\",\"HomeExternalID\":\"\",\"ItemType\":\"R\",\"LinkedItem\":\"\",\"MergeGUID\":\"\",\"MergeProf\":\"\",\"NonSubscrMode\":\"N\",\"OfflineMode\":\"\",\"OpenClaimPrice\":\"\",\"OpenContext\":\"\",\"OriginalLabor\":\"\",\"OriginalPrice\":\"\",\"PDCopy\":\"0\",\"ProfileId\":\"{ProfileID_2}\",\"PxnResearchMethod\""
		":\"\",\"ReinspActiveFlag\":\"0\",\"ValActiveFlag\":\"1\",\"ValReport\":\"yes\",\"ViewDialog\":\"\"}}", 
		LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T19_ViewReport_ST03_Service_OpenClaim", LR_AUTO);

//	web_concurrent_start(NULL);
	
	lr_start_sub_transaction("RMS_AutoSource_T19_ViewReport_ST04_Page_RptViewer", "RMS_AutoSource_T19_ViewReport");

	web_custom_request("RptViewer", 
		"URL=https://adxe-qa.apps-dev.gp2.axadmin.net/api/Page/RptViewer", 
		"Method=GET", 
		"Resource=1", 
		"RecContentType=text/plain", 
		"Referer=https://adxe-qa.apps-dev.gp2.axadmin.net/Estimating/Autosource/ValuationReports", 
		"Snapshot=t47.inf", 
		LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T19_ViewReport_ST04_Page_RptViewer", LR_AUTO);

	
	lr_start_sub_transaction("RMS_AutoSource_T19_ViewReport_ST05_Page_Reports", "RMS_AutoSource_T19_ViewReport");
	
	//OnClick":"OnShowReport('AS-53694915')

	web_reg_save_param_ex(
		"ParamName=Report_ID",
		"LB=OnClick\":\"OnShowReport('",
		"RB=')",
		"Ordinal=1",
		SEARCH_FILTERS,
		LAST);


	web_custom_request("Reports", 
		"URL=https://adxe-qa.apps-dev.gp2.axadmin.net/api/Page/Reports?Type=ASReport", 
		"Method=GET", 
		"Resource=1", 
		"RecContentType=text/plain", 
		"Referer=https://adxe-qa.apps-dev.gp2.axadmin.net/Estimating/Autosource/ValuationReports", 
		"Snapshot=t50.inf", 
		LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T19_ViewReport_ST05_Page_Reports", LR_AUTO);

//	web_concurrent_end(NULL);
	
	lr_start_sub_transaction("RMS_AutoSource_T19_ViewReport_ST06_Service_GetReport", "RMS_AutoSource_T19_ViewReport");
	
	web_reg_save_param_ex("ParamName=PDF_ID", "LB=", "RB=.pdf",
				SEARCH_FILTERS,
				"Scope=Body",
				"IgnoreRedirections=No",
				LAST);

	web_custom_request("GetReport", 
		"URL=https://adxe-qa.apps-dev.gp2.axadmin.net/api/Service/GetReport?Type={Report_ID}", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=text/plain", 
		"Referer=https://adxe-qa.apps-dev.gp2.axadmin.net/Estimating/Autosource/ValuationReports", 
		"Snapshot=t51.inf", 
		"Mode=HTTP", 
		"EncType=application/json;charset=UTF-8", 
		"Body={\"Form\":{}}", 
		LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T19_ViewReport_ST06_Service_GetReport", LR_AUTO);

	web_add_cookie("ticket="
		"ADPFalcon%3DADPAppMode%3DEst%26ASWebOnly%3DN%26ADPRsdlInvMsg%3D0%26PrtResearchMode%3D%26FalconTicket%3D{platFormToken}%26ADXEMode%3D1%26USENGLISH%3DUSENGLISH%26ASSpecialtyVehicle%3Dno%26ADPReadyFromClose%3D%26DupMsg%3D0%26SFStaffBlockSuppDRP%3D%26SpecialtySet%3Dno%26SingleValuation%3D0%26ADPcompanycode%3D%26ViewerPermCall%3D%26ADPListMode%3D%26ADPUSRExternalID%3D{ProfileID_2}%26QuickEstMode%3D1%26ADPReadOnlyControls%3D0%26ADPEstID%3D0%26MoveWorkSharingFlg%3D0%26GlobalRatesFla"
		"g%3D0%26ADPReadOnlyValControls%3D0%26ADPClaimGuid%3D7587439F%252DBD17%252D498A%252D9FA1%252D73EF4CA2612A%26CallPQEQuoteId%3D0%26ADPContext%3DReport%26ADPOrder%3DD%26ASHeavyTruckMapping%3D1%252C2%252C3%26DmgLstSortType%3D0%26DmgLstSortCol%3D1%26ADPVinRpt%3D1%26ADPPaintWarning%3D1%26ADPExternalID%3D{ProfileID_2}%26ADPUserID%3D{UserID}%26ADPTz%3D0%26NonSubscr%3D%26ADPcountrycode%3DUs%26ADPWorklistFilterRO%3D0%26ADPWorklistFilter%3D%26ADPProfile%3DAll%26ProdAdded%3D0%26ADPMrg%3D0%26CurrentVehSect"
		"ion%3D%26ADPTLWMessage%3D0%26ConvertDlg%3D1%26PreUpdMode%3D0%26PreUpdReqNbr%3D%26PreUpdRepNbr%3D446124%26PreUpdReinsp%3D0%26ADPUpdateDialog%3D0%26TLMAuthToken%3D%26Pentools%3D0%26ClaimsListPageNo%3D1%26ADPSHPExternalID%3D{ProfileID_3}%26ADPLdapUIDName%3D%26ADPLdapUserOrgName%3D{ProfileID_3}%26GlobalVehicle%3D0%26PostToSync%3D0%26ValReqID%3D0%26ADPVehTypeMessage%3D0%26OpenEstMode%3D0%26ADPCrashMessage%3DX%26TLMPosted%3D%26UpdValOrigProdInfo%3D%26PreUpdExtID%3D{ProfileID_3}%26ADPAllMode%3D1%26ADPEstimateEnabled%3D1%2"
		"6ADPLdapSessionTimeOut%3D3600%26NumUpdProfiles%3D1%26ADPOpenMode%3D0%26ADPAutoDecode%3D1%26ShowWhatsNew%3D1%26ADPValVehNeeded%3D0%26ADPCatViewEnabled%3D0%26AutoImport%3D0%26ADPColumn%3DCreateDateTime%255FDate%26ADPlob%3DSHP%26ClientAppVersion%3D10%252E03%252E440%253B; DOMAIN=adxe-qa.apps-dev.gp2.axadmin.net");

	lr_start_sub_transaction("RMS_AutoSource_T19_ViewReport_ST07_Page_Admin", "RMS_AutoSource_T19_ViewReport");
	
	web_custom_request("Admin", 
		"URL=https://adxe-qa.apps-dev.gp2.axadmin.net/api/Page/Admin", 
		"Method=GET", 
		"Resource=1", 
		"RecContentType=text/plain", 
		"Referer=https://adxe-qa.apps-dev.gp2.axadmin.net/Estimating/Autosource/ValuationReports", 
		"Snapshot=t52.inf", 
		LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T19_ViewReport_ST07_Page_Admin", LR_AUTO);

	
	web_reg_find("Text=%PDF-",
		LAST);
	
	lr_start_sub_transaction("RMS_AutoSource_T19_ViewReport_ST08_Session_GetFile", "RMS_AutoSource_T19_ViewReport");

	web_custom_request("Get", 
		"URL=https://adxe-qa.apps-dev.gp2.axadmin.net/api/Session/Get?file={PDF_ID}.pdf", 
		"Method=GET", 
		"Resource=1", 
		"RecContentType=application/pdf", 
		"Referer=https://adxe-qa.apps-dev.gp2.axadmin.net/Estimating/Autosource/ValuationReports", 
		"Snapshot=t56.inf", 
		LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T19_ViewReport_ST08_Session_GetFile", LR_AUTO);

	lr_start_sub_transaction("RMS_AutoSource_T19_ViewReport_ST09_Service_UnLockFile", "RMS_AutoSource_T19_ViewReport");
	
	web_custom_request("LockFile_2", 
		"URL=https://adxe-qa.apps-dev.gp2.axadmin.net/api/Service/LockFile", 
		"Method=POST", 
		"Resource=0", 
		"RecContentType=text/plain", 
		"Referer=https://adxe-qa.apps-dev.gp2.axadmin.net/Estimating/Autosource/ValuationReports", 
		"Snapshot=t60.inf", 
		"Mode=HTTP", 
		"EncType=application/json;charset=UTF-8", 
		"Body={\"Form\":{\"Request\":{\"@Method\":\"UnlockFile\",\"GUID\":\"{ReportGUID}\",\"fileType\":\"Claims\"}}}", 
		LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T19_ViewReport_ST09_Service_UnLockFile", LR_AUTO);

	
lr_end_transaction("RMS_AutoSource_T19_ViewReport", LR_AUTO);

return 0;
}