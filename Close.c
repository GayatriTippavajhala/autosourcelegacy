Close()
{

lr_start_transaction("RMS_AutoSource_T15_ClickClose1");

	web_add_header("Origin", "{ADXE_URL}");
	
	lr_start_sub_transaction("RMS_AutoSource_T15_ClickClose1_ST00_Service_ClaimUpdate", "RMS_AutoSource_T15_ClickClose1");

		web_custom_request("ClaimUpdate_9", 
			"URL={ADXE_URL}/api/Service/ClaimUpdate", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/OtherInformation", 
			"Snapshot=t85.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"GUID\":[{\"name\":\"GUID\",\"caption\":\"{GUID}\",\"value\":\"{ClaimGUID}\"},{\"name\":\"GUID\",\"caption\":\"{GUID}\",\"value\":\"{ClaimGUID}\"}],\"ProductId\":{\"fieldtype\":\"hiddendata\",\"name\":\"ProductId\",\"caption\":\"{ProductId}\",\"value\":null},\"EstimateActiveFlag\":{\"fieldtype\":\"hiddendata\",\"name\":\"EstimateActiveFlag\",\"caption\":\"{EstimateActiveFlag}\",\"value\":\"1\"},\"AutoSourceActiveFlag\":{\""
			"fieldtype\":\"hiddendata\",\"name\":\"AutoSourceActiveFlag\",\"caption\":\"{AutoSourceActiveFlag}\",\"value\":\"1\"},\"TSHidNoTax\":{\"fieldtype\":\"hiddendata\",\"name\":\"TSHidNoTax\",\"caption\":\"{TSHidNoTax}\",\"value\":\"\"},\"TSZipCode\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"aspostal\",\"maxlength\":\"9\",\"onchange\":\"DecodeTaxZip();\",\"name\":\"TSZipCode\",\"caption\":\"Zip Code:\",\"value\":\"{ZipCode}-\",\"mandatory\":\"true\"},\"TSCityCounty\":{\"fieldset\":\""
			"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:100%;\",\"onchange\":\"OnCityCounty()\",\"spaces\":\"yes\",\"name\":\"TSCityCounty\",\"caption\":\"City and County:\",\"combovalue\":[{\"@value\":\"\",\"@abbrev\":\"\"},{\"@value\":\"unknown\",\"@abbrev\":\"\",\"#text\":\"Unknown-Unknown\"},{\"@phrase\":\"IRVING~~DALLAS\",\"@value\":\"441131490^irvingdallas\",\"#text\":\"Irving - Dallas\"},{\"@phrase\":\"~~DALLAS\",\"@value\":\"441130000^dallas\",\"#text\":\"Dallas County\"}],\"value\":\""
			"441131490^irvingdallas\"},\"TSCity\":{\"fieldtype\":\"hiddendata\",\"name\":\"TSCity\",\"caption\":\"{TSCity}\",\"value\":\"{TaxCity}\"},\"TSCounty\":{\"fieldtype\":\"hiddendata\",\"name\":\"TSCounty\",\"caption\":\"{TSCounty}\",\"value\":\"DALLAS\"},\"TSExemptSt\":{\"fieldtype\":\"hiddendata\",\"name\":\"TSExemptSt\",\"caption\":\"{TSExemptSt}\",\"value\":\"\"},\"TSOverrideRate\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"percent3\",\"size\":\"5\",\"maxlength\":\"5\",\"Format"
			"\":\"2.3\",\"name\":\"TSOverrideRate\",\"caption\":\"Override Rate:\",\"value\":null},\"TSOverrideAuthBy\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"string\",\"maxlength\":\"15\",\"style\":\"width:100%\",\"name\":\"TSOverrideAuthBy\",\"caption\":\"Override Authorized By:\",\"value\":null},\"TSOverrideFlag\":{\"fieldtype\":\"hiddendata\",\"name\":\"TSOverrideFlag\",\"caption\":\"{TSOverrideFlag}\",\"value\":null},\"TSTempOverrideFlag\":{\"fieldtype\":\"checkbox_single\",\""
			"OnClick\":\"ChkTaxOverride()\",\"name\":\"TSTempOverrideFlag\",\"caption\":\"Override:\",\"value\":null},\"TSEnableTax\":{\"fieldtype\":\"hiddendata\",\"name\":\"TSEnableTax\",\"caption\":\"{TSEnableTax}\",\"value\":\"1\"},\"TSEnableTaxOverride\":{\"fieldtype\":\"hiddendata\",\"name\":\"TSEnableTaxOverride\",\"caption\":\"{TSEnableTaxOverride}\",\"value\":\"1\"},\"OIAdjPolarity\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:100%\",\"name\":\"OIAdjPolarity\",\"caption\":"
			"\"Other Adjustment:\",\"combovalue\":[{\"@value\":\"add\",\"#text\":\"+ Add\"},{\"@value\":\"deduct\",\"#text\":\"- Deduct\"}],\"value\":\"add\"},\"OIAdjValue\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"ascurrency\",\"size\":\"6\",\"maxlength\":\"6\",\"Format\":\"6.0\",\"name\":\"OIAdjValue\",\"caption\":\"Other Adjustment Value:\",\"value\":\"0\"},\"OIAdjDescription\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"string\",\"maxlength\":\"40\",\"style\""
			":\"width:100%\",\"name\":\"OIAdjDescription\",\"caption\":\"Description:\",\"value\":null},\"OIValuationNotes\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"textarea\",\"datatype\":\"string\",\"rows\":\"10\",\"cols\":\"75\",\"onpaste\":\"onNotesPaste(this);\",\"name\":\"OIValuationNotes\",\"caption\":\"Valuation Notes:\",\"value\":null},\"OIAttention\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"string\",\"maxlength\":\"25\",\"style\":\"width:100%\",\"name\":\"OIAttention\",\""
			"caption\":\"Attention:\",\"value\":null},\"OITransferFee\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"ascurrency\",\"size\":\"4\",\"maxlength\":\"4\",\"Format\":\"4.0\",\"name\":\"OITransferFee\",\"caption\":\"Transfer Fee:\",\"value\":null},\"OITitleFee\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"ascurrency\",\"size\":\"4\",\"maxlength\":\"4\",\"Format\":\"4.0\",\"name\":\"OITitleFee\",\"caption\":\"Title Fee:\",\"value\":null},\"OISalvageType\":{\""
			"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:100%\",\"name\":\"OISalvageType\",\"caption\":\"Salvage Title:\",\"combovalue\":[{\"@value\":\"0\",\"#text\":\"No Salvage Title\"},{\"@value\":\"1\",\"#text\":\"Not Noticeable\"},{\"@value\":\"2\",\"#text\":\"Noticeable from Interior\"},{\"@value\":\"3\",\"#text\":\"Noticeable from Exterior\"}],\"value\":null},\"OISpecialInstructions\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"textarea\",\"datatype\":\"string\",\"rows\":\"2\",\""
			"cols\":\"75\",\"onpaste\":\"onNotesPaste(this);\",\"name\":\"OISpecialInstructions\",\"caption\":\"Special Instructions:\",\"value\":null},\"MAZipCode\":{\"fieldtype\":\"hiddendata\",\"name\":\"MAZipCode\",\"caption\":\"Zip Code:\",\"value\":\"{ZipCode}\"},\"TLReinspectionMode\":{\"fieldtype\":\"hiddendata\",\"name\":\"TLReinspectionMode\",\"caption\":\"{TLReinspectionMode}\",\"value\":\"N\"},\"ReqReady\":{\"#text\":\"1\",\"value\":null},\"ClaimNo\":{\"@custom\":\"1\",\"fieldset\":\"Estimate\",\""
			"fieldtype\":\"input\",\"datatype\":\"claimnumber\",\"size\":\"20\",\"CustId\":\"APL\",\"mask\":\"\",\"maskEmpty\":\"\",\"maskSize\":\"25\",\"name\":\"ClaimNo\",\"caption\":\"Claim # :\",\"value\":\"LT_AS_{ClaimRandNum}\"},\"EstimateStatus\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"hiddendata\",\"name\":\"EstimateStatus\",\"caption\":\"Status:\",\"combovalue\":[{\"@value\":\"0\",\"#text\":\"Assigned\"},{\"@value\":\"1\",\"#text\":\"In Progress\"},{\"@value\":\"2\",\"#text\":\"Completed Estimate Only\"},{"
			"\"@value\":\"3\",\"#text\":\"Completed Estimate, Notes, Forms\"},{\"@value\":\"4\",\"#text\":\"Template\"},{\"@value\":\"5\",\"#text\":\"Assgn Dup (Assignment Duplicate)\"},{\"@value\":\"6\",\"#text\":\"Assgn Comp (Assignment Completed)\"},{\"@value\":\"7\",\"#text\":\"Ready\"},{\"@value\":\"8\",\"#text\":\"Est Dup (Estimate Duplicate)\"},{\"@value\":\"9\",\"#text\":\"Report\"},{\"@value\":\"10\",\"#text\":\"Assgn Supp\"},{\"@value\":\"11\",\"#text\":\"Cancelled\"},{\"@value\":\"12\",\"#text\":\""
			"Pending Approval\"},{\"@value\":\"13\",\"#text\":\"Not Approved\"},{\"@value\":\"14\",\"#text\":\"Approved\"},{\"@value\":\"15\",\"#text\":\"Rev Assigned\"},{\"@value\":\"16\",\"#text\":\"Update In Progress\"},{\"@value\":\"17\",\"#text\":\"Agreement Pending\"},{\"@value\":\"18\",\"#text\":\"Update Completed\"},{\"@value\":\"19\",\"#text\":\"Completed with Revisions\"},{\"@value\":\"20\",\"#text\":\"Assigned Reinspection\"},{\"@value\":\"21\",\"#text\":\"In Progress Reinspection\"},{\"@value\":\""
			"22\",\"#text\":\"Completed as Reference\"},{\"@value\":\"23\",\"#text\":\"Imported\"},{\"@value\":\"24\",\"#text\":\"Completed as Record\"},{\"@value\":\"25\",\"#text\":\"Sent for Reinspection\"},{\"@value\":\"26\",\"#text\":\"Retrieved\"},{\"@value\":\"27\",\"#text\":\"Return Review\"},{\"@value\":\"28\",\"#text\":\"Supp Notice\"}],\"value\":\"0\"},\"Label_VehDescription\":{\"name\":\"Label_VehDescription\",\"caption\":\"Desc:\",\"value\":null},\"VehicleDescription\":{\"fieldset\":\"Estimate\",\""
			"datatype\":\"string\",\"name\":\"VehicleDescription\",\"caption\":\"Vehicle Description:\",\"value\":\"{Veh_Desc1}\"},\"ValuationStatus\":{\"fieldtype\":\"statictrans\",\"name\":\"ValuationStatus\",\"caption\":\"Status:\",\"combovalue\":[{\"@value\":\"0\",\"#text\":\"In Progress\"},{\"@value\":\"1\",\"#text\":\"In Progress\"},{\"@value\":\"3\",\"#text\":\"Completed\"},{\"@value\":\"7\",\"#text\":\"Ready\"},{\"@value\":\"WIP\",\"#text\":\"Work In Progress\"},{\"@value\":\"COM\",\"#text\":\""
			"Completed\"},{\"@value\":\"CAN\",\"#text\":\"Cancelled\"},{\"@value\":\"RCV\",\"#text\":\"Received\"},{\"@value\":\"FOR\",\"#text\":\"Forwarded\"}],\"value\":\"1\"},\"Check_CommitValuation\":{\"fieldtype\":\"checkbox_single\",\"OnClick\":\"onSelectReady()\",\"name\":\"Check_CommitValuation\",\"caption\":\"Request Ready\",\"value\":null},\"ProdDescription\":{\"fieldtype\":\"static\",\"name\":\"ProdDescription\",\"caption\":\"Product Desc:\",\"value\":\"Test Prod For TLDG\"},\"ShoplinkLiteVal\":{\""
			"fieldtype\":\"hiddendata\",\"name\":\"ShoplinkLiteVal\",\"caption\":\"{ShoplinkLiteVal}\",\"value\":null},\"BaseXML\":\"ASOtherInfo.xml\",\"PageName\":\"ASOtherInfo\",\"BaseXMLType\":\"Val\",\"SaveData\":\"1\"}}", 
			LAST);

	lr_end_sub_transaction("RMS_AutoSource_T15_ClickClose1_ST00_Service_ClaimUpdate", LR_AUTO);


	lr_start_sub_transaction("RMS_AutoSource_T15_ClickClose1_ST01_Page_CloseClaim", "RMS_AutoSource_T15_ClickClose1");
	
		web_url("CloseClaim", 
			"URL={ADXE_URL}/api/Page/CloseClaim", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
			"Snapshot=t86.inf", 
			"Mode=HTML", 
			LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T15_ClickClose1_ST01_Page_CloseClaim", LR_AUTO);
	
	lr_start_sub_transaction("RMS_AutoSource_T15_ClickClose1_ST02_Page_Admin", "RMS_AutoSource_T15_ClickClose1");
	
		web_url("Admin_2", 
			"URL={ADXE_URL}/api/Page/Admin", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
			"Snapshot=t87.inf", 
			"Mode=HTML", 
			LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T15_ClickClose1_ST02_Page_Admin", LR_AUTO);
	
//	lr_start_transaction("AS_CiecaSettings_CiecaAclUrl");
//		web_url("CiecaAclUrl_2", 
//			"URL={ADXE_URL}/api/CiecaSettings/CiecaAclUrl", 
//			"Resource=0", 
//			"RecContentType=text/plain", 
//			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
//			"Snapshot=t88.inf", 
//			"Mode=HTML", 
//			LAST);
//	lr_end_transaction("AS_CiecaSettings_CiecaAclUrl", LR_AUTO);

	
	lr_start_sub_transaction("RMS_AutoSource_T15_ClickClose1_ST03_Page_ROAssignInfo", "RMS_AutoSource_T15_ClickClose1");
	
		web_url("ROAssignInfo", 
			"URL={ADXE_URL}/api/Page/ROAssignInfo", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
			"Snapshot=t89.inf", 
			"Mode=HTML", 
			LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T15_ClickClose1_ST03_Page_ROAssignInfo", LR_AUTO);


	lr_start_sub_transaction("RMS_AutoSource_T15_ClickClose1_ST04_Page_Vehicle", "RMS_AutoSource_T15_ClickClose1");
	
		web_reg_save_param_ex("ParamName=ClaimNum_Cap", "LB=\"Claim # :\",\"value\":\"", "RB=\"}",
			SEARCH_FILTERS,
			"Scope=BODY",
			LAST);
	
		web_url("Vehicle_3", 
			"URL={ADXE_URL}/api/Page/Vehicle", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
			"Snapshot=t90.inf", 
			"Mode=HTML", 
			LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T15_ClickClose1_ST04_Page_Vehicle", LR_AUTO);
	
	
	web_add_auto_header("Sec-Fetch-Site", "same-site");

//	Cieca_Export();

lr_end_transaction("RMS_AutoSource_T15_ClickClose1",LR_AUTO);

	
lr_start_transaction("RMS_AutoSource_T16_ClickClose2");

	web_add_auto_header("Sec-Fetch-Site", "same-origin");

//	lr_start_transaction("AS_Page_MandatoryFieldsDlg_AppMode");
	
	lr_start_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST00_Page_MandatoryFieldsDlg", "RMS_AutoSource_T16_ClickClose2");
	
		web_url("MandatoryFieldsDlg", 
			"URL={ADXE_URL}/api/Page/MandatoryFieldsDlg?AppMode=Est&FinalBill=&EstComplete=1&SalvAssign=", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
			"Snapshot=t92.inf", 
			"Mode=HTML", 
			LAST);
//	lr_end_transaction("AS_Page_MandatoryFieldsDlg_AppMode", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST00_Page_MandatoryFieldsDlg", LR_AUTO);


	web_add_auto_header("Origin", "{ADXE_URL}");

//	lr_start_transaction("AS_Service_GetClaimSvr");
	
	lr_start_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST01_Service_GetClaimSvr", "RMS_AutoSource_T16_ClickClose2");
	
		web_custom_request("GetClaimSvr", 
			"URL={ADXE_URL}/api/Service/GetClaimSvr", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
			"Snapshot=t93.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"Request\":{\"@Method\":\"FixDamages\"}}}", 
			LAST);
//	lr_end_transaction("AS_Service_GetClaimSvr", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST01_Service_GetClaimSvr", LR_AUTO);


//	lr_start_transaction("AS_Service_ValidateTLWS");
	
	lr_start_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST02_Service_ValidateTLWS", "RMS_AutoSource_T16_ClickClose2");
	
		web_custom_request("ValidateTLWS", 
			"URL={ADXE_URL}/api/Service/ValidateTLWS", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
			"Snapshot=t94.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"Request\":{\"@Method\":\"validate\",\"EstStat\":10}}}", 
			LAST);
//	lr_end_transaction("AS_Service_ValidateTLWS", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST02_Service_ValidateTLWS", LR_AUTO);


//	lr_start_transaction("AS_Service_MandatoryFieldsDlgUpdate");
	
	lr_start_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST03_Service_MandatoryFieldsDlgUpdate", "RMS_AutoSource_T16_ClickClose2");
	
		web_custom_request("MandatoryFieldsDlgUpdate", 
			"URL={ADXE_URL}/api/Service/MandatoryFieldsDlgUpdate", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
			"Snapshot=t95.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"BaseXML\":\"MandFieldsOpenClaim.xml\",\"SaveData\":\"1\",\"Insured_Last\":{\"name\":\"Insured_Last\",\"value\":\"{Lastname_own}\"},\"GUID\":{\"name\":\"GUID\",\"value\":\"{ClaimGUID}\"},\"InspTypeText\":{\"name\":\"InspTypeText\",\"value\":\"AARP:::CATD:::CATF:::DAP:::DRP:::D:::F:::FIS:::ID:::IF:::MAC:::OHC:::PRO:::RSP:::UNK:::WAV\"},\"LossTypeText\":{\"name\":\"LossTypeText\",\"value\":\"\"},\"LossDateTime_Date\":{\"name\":\"LossDateTime_Date\",\"value\":\"{LossDate}\"}"
			",\"InspDateTime_Date\":{\"name\":\"InspDateTime_Date\",\"value\":\"{InspDate}\"},\"InspLocation\":{\"name\":\"InspLocation\",\"value\":\"\"},\"InspCity\":{\"name\":\"InspCity\",\"value\":\"\"},\"InspZipCode\":{\"name\":\"InspZipCode\",\"value\":\"\"},\"InspState\":{\"name\":\"InspState\",\"value\":\"\"},\"AssignedDateTime_Date\":{\"name\":\"AssignedDateTime_Date\",\"value\":\"\"},\"ReceivedDateTime_Date\":{\"name\":\"ReceivedDateTime_Date\",\"value\":\"\"},\"FirstContactDateTime_Date\":{\""
			"name\":\"FirstContactDateTime_Date\",\"value\":\"\"},\"SecondContactDateTime_Date\":{\"name\":\"SecondContactDateTime_Date\",\"value\":\"\"},\"ApptDateTime_Date\":{\"name\":\"ApptDateTime_Date\",\"value\":\"\"},\"ScheduledOutDateTime_Date\":{\"name\":\"ScheduledOutDateTime_Date\",\"value\":\"\"},\"RepairStartDateTime_Date\":{\"name\":\"RepairStartDateTime_Date\",\"value\":\"\"},\"RepairCompleteDateTime_Date\":{\"name\":\"RepairCompleteDateTime_Date\",\"value\":\"\"},\"VehDropoffDateTime_Date\":{\""
			"name\":\"VehDropoffDateTime_Date\",\"value\":\"\"},\"VehPickupDateTime_Date\":{\"name\":\"VehPickupDateTime_Date\",\"value\":\"\"},\"ClaimNumberMask\":{\"name\":\"ClaimNumberMask\",\"value\":\"\"},\"Deductible\":{\"name\":\"Deductible\",\"value\":\"$500.00\"},\"DeductibleReason\":{\"name\":\"DeductibleReason\",\"value\":\"\"},\"CreatedForProfileId\":{\"name\":\"CreatedForProfileId\",\"value\":\"{Profile_Id}\"},\"AppMode\":{\"name\":\"AppMode\",\"value\":[\"\",\"Est\"]},\"MandXML\":{\"name\":\""
			"MandXML\",\"value\":\"\"},\"CustomAdminDom\":{\"name\":\"CustomAdminDom\",\"value\":\"\"},\"Item_Type\":{\"name\":\"Item_Type\",\"value\":\"E\"},\"icbcMode\":{\"name\":\"icbcMode\",\"value\":\"N\"},\"CatastropheIDFlag\":{\"name\":\"CatastropheIDFlag\",\"value\":\"0\"},\"CatastropheID\":{\"name\":\"CatastropheID\",\"value\":\"\"},\"LHPDayOfRepairPerm\":{\"name\":\"LHPDayOfRepairPerm\",\"value\":\"0\"}}}", 
			LAST);
//	lr_end_transaction("AS_Service_MandatoryFieldsDlgUpdate", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST03_Service_MandatoryFieldsDlgUpdate", LR_AUTO);


//	lr_start_transaction("AS_Service_AsValidateclaim");
	
	lr_start_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST04_Service_AsValidateClaim", "RMS_AutoSource_T16_ClickClose2");
	
		web_custom_request("ASValidateClaim", 
			"URL={ADXE_URL}/api/Service/ASValidateClaim", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
			"Snapshot=t96.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{}}", 
			LAST);
//	lr_end_transaction("AS_Service_AsValidateclaim", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST04_Service_AsValidateClaim", LR_AUTO);


//	lr_start_transaction("AS_Service_CreatePrtImages_SaveRoAssign");
	
	lr_start_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST05_Service_CreatePrtImages_SaveRoAssign", "RMS_AutoSource_T16_ClickClose2");
	
		web_custom_request("CreatePrtImages", 
			"URL={ADXE_URL}/api/Service/CreatePrtImages?GUID={ClaimGUID}", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
			"Snapshot=t97.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"Request\":{\"Method\":\"SaveRoAssign\",\"AssignROFlag\":\"2\",\"RONumber\":\"\",\"ROStatus\":\"\",\"ROPaymentIns\":\"\",\"ROPaymentOwner\":\"\",\"ROPaymentOther\":\"\"}}}", 
			LAST);
//	lr_end_transaction("AS_Service_CreatePrtImages_SaveRoAssign", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST05_Service_CreatePrtImages_SaveRoAssign", LR_AUTO);


//	lr_start_transaction("AS_Service_CreatePrtImages_CheckForDuplicate");
	
	lr_start_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST06_Service_CreatePrtImages_CheckForDuplicate", "RMS_AutoSource_T16_ClickClose2");

		web_custom_request("CreatePrtImages_2", 
			"URL={ADXE_URL}/api/Service/CreatePrtImages?GUID={ClaimGUID}", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
			"Snapshot=t98.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
//			"Body={\"Form\":{\"Request\":{\"Method\":\"CheckForDuplicate\",\"Valuation\":\"1\",\"EstimateReview\":\"\",\"Claim\":{\"GUID\":\"{ClaimGUID}\",\"Windows\":\"false\",\"RefBrk\":\"false\",\"SubstrateDetail\":\"1\",\"FinalBill\":\"false\",\"Language\":\"USENGLISH\",\"Disclaimer\":null,\"Disclosure\":\"TX\",\"InsStateDisclosure\":[\"TX\",null],\"CloseStatus\":10,\"MultipleHeadersID\":null}}}}", 
			"Body={\"Form\":{\"Request\":{\"Method\":\"CheckForDuplicate\",\"Valuation\":\"1\",\"EstimateReview\":\"\",\"Claim\":{\"GUID\":\"{ClaimGUID}\",\"Windows\":\"false\",\"RefBrk\":\"false\",\"SubstrateDetail\":\"1\",\"FinalBill\":\"false\",\"Language\":\"USENGLISH\",\"Disclaimer\":null,\"Disclosure\":\"AL\",\"InsStateDisclosure\":\"\",\"CloseStatus\":10,\"MultipleHeadersID\":null}}}}",
			LAST);

	
	
//	lr_end_transaction("AS_Service_CreatePrtImages_CheckForDuplicate", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST06_Service_CreatePrtImages_CheckForDuplicate", LR_AUTO);

//	lr_start_transaction("AS_Service_CreatePrtImages_BuildRptList");
	
	lr_start_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST07_Service_CreatePrtImages_BuildRptList", "RMS_AutoSource_T16_ClickClose2");
	
		web_custom_request("CreatePrtImages_3", 
			"URL={ADXE_URL}/api/Service/CreatePrtImages?GUID={ClaimGUID}&CreateImgFlg=1", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
			"Snapshot=t99.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"Request\":{\"Method\":\"BuildRptList\"}}}", 
			LAST);
//	lr_end_transaction("AS_Service_CreatePrtImages_BuildRptList", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST07_Service_CreatePrtImages_BuildRptList", LR_AUTO);
	
//	lr_start_transaction("AS_Service_CreatePrtImages_CreatePrtImg");
	
	lr_start_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST08_Service_CreatePrtImages_CreatePrtImg", "RMS_AutoSource_T16_ClickClose2");
	
		web_custom_request("CreatePrtImages_4", 
			"URL={ADXE_URL}/api/Service/CreatePrtImages?GUID={ClaimGUID}&RptType=&EstRev=", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
			"Snapshot=t100.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"Request\":{\"Method\":\"CreatePrtImg\",\"CloseStatus\":\"10\"}}}", 
			LAST);
//	lr_end_transaction("AS_Service_CreatePrtImages_CreatePrtImg", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST08_Service_CreatePrtImages_CreatePrtImg", LR_AUTO);
	
//	lr_start_transaction("AS_Service_CreatePrtImages_SendEDISoleraNotify");	
	
	lr_start_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST09_Service_CreatePrtImages_SendEDISoleraNotify", "RMS_AutoSource_T16_ClickClose2");
	
		web_custom_request("CreatePrtImages_5", 
			"URL={ADXE_URL}/api/Service/CreatePrtImages?GUID={ClaimGUID}&NotifyEDI=&EstRev=&Lang=USENGLISH", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
			"Snapshot=t101.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"Request\":{\"Method\":\"SendEDISoleraNotify\"}}}", 
			LAST);
//	lr_end_transaction("AS_Service_CreatePrtImages_SendEDISoleraNotify", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST09_Service_CreatePrtImages_SendEDISoleraNotify", LR_AUTO);
	
//	lr_start_transaction("AS_Service_SetClaimStatus_1");		
	
	lr_start_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST10_Service_SetClaimStatus1", "RMS_AutoSource_T16_ClickClose2");
	
		web_custom_request("SetClaimStatus", 
			"URL={ADXE_URL}/api/Service/SetClaimStatus", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
			"Snapshot=t102.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"ASAltRptEnableFlag\":0,\"ASAltRptLang\":0,\"ASNotCloseFlag\":\"\",\"AssignROFlag\":\"0\",\"CloseFlag\":\"\",\"CustDBUpdateFlag\":null,\"DestUrl\":\"\",\"EstCloseStatus\":10,\"EstimateReview\":\"\",\"FinalBill\":0,\"LinkedItem\":\"\",\"LinkedStatus\":\"\",\"RONumber\":\"\",\"ROPaymentIns\":\"\",\"ROPaymentOther\":\"\",\"ROPaymentOwner\":\"\",\"ROStatus\":\"\",\"ReqMethod\":\"\",\"ShoplinkLiteVal\":\"\",\"ValCloseStatus\":11,\"ValMSStatus\":\"0\",\"VinCheckFlag\":\"1\",\""
			"VinDatashareFlag\":\"1\"}}", 
			LAST);
//	lr_end_transaction("AS_Service_SetClaimStatus_1", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST10_Service_SetClaimStatus1", LR_AUTO);

	
//	lr_start_transaction("AS_Service_SetClaimStatus_2");	
	
	lr_start_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST11_Service_SetClaimStatus2", "RMS_AutoSource_T16_ClickClose2");
	
		web_custom_request("SetClaimStatus_2",
			"URL={ADXE_URL}/api/Service/SetClaimStatus", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
			"Snapshot=t103.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"ASAltRptEnableFlag\":0,\"ASAltRptLang\":0,\"ASNotCloseFlag\":\"\",\"AssignROFlag\":\"\",\"CloseFlag\":\"1\",\"CustDBUpdateFlag\":null,\"DestUrl\":\"ClaimsList.asp\",\"EstCloseStatus\":10,\"EstimateReview\":\"\",\"FinalBill\":0,\"LinkedItem\":\"\",\"LinkedStatus\":\"\",\"RONumber\":\"\",\"ROPaymentIns\":\"\",\"ROPaymentOther\":\"\",\"ROPaymentOwner\":\"\",\"ROStatus\":\"\",\"ReqMethod\":\"final\",\"ShoplinkLiteVal\":\"\",\"ValCloseStatus\":11,\"ValMSStatus\":\"0\",\"VinCheckFlag"
			"\":\"1\",\"VinDatashareFlag\":\"1\"}}", 
			LAST);
//	lr_end_transaction("AS_Service_SetClaimStatus_2", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST11_Service_SetClaimStatus2", LR_AUTO);

	web_revert_auto_header("Origin");
	
//	lr_start_transaction("AS_CiecaSettings_CiecaAclUrl");	
//		web_url("CiecaAclUrl_3", 
//			"URL={ADXE_URL}/api/CiecaSettings/CiecaAclUrl", 
//			"Resource=0", 
//			"RecContentType=text/plain", 
//			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
//			"Snapshot=t104.inf", 
//			"Mode=HTML", 
//			LAST);
//	lr_end_transaction("AS_CiecaSettings_CiecaAclUrl", LR_AUTO);
//
//	Cieca_Export();

	web_add_auto_header("Sec-Fetch-Site", "same-origin");
	
//	lr_start_transaction("AS_Service_UnlockFile");
	
	lr_start_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST12_Service_UnlockFile", "RMS_AutoSource_T16_ClickClose2");
	
		web_custom_request("LockFile_2", 
			"URL={ADXE_URL}/api/Service/LockFile", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Complete", 
			"Snapshot=t106.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"Request\":{\"@Method\":\"UnlockFile\",\"GUID\":\"{ClaimGUID}\",\"fileType\":\"Claims\"}}}", 
			LAST);
//	lr_end_transaction("AS_Service_UnlockFile", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST12_Service_UnlockFile", LR_AUTO);

//	lr_start_transaction("AS_Page_ClaimsList");	
	
	lr_start_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST13_Page_ClaimsList", "RMS_AutoSource_T16_ClickClose2");
	
		web_custom_request("ClaimsList_2", 
			"URL={ADXE_URL}/api/Page/ClaimsList", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=application/json", 
			"Referer={ADXE_URL}/Estimating/WorkList", 
			"Snapshot=t107.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"ApptDateTime_Date\":\"\",\"ApptDateTime_Time\":\"\",\"Header_Filter\":\"\",\"Header_Filter_RO\":0,\"Header_Profiles\":\"All\",\"ItemsPerPage\":\"\",\"PageNo\":1,\"SortKey\":\"\"}}", 
			LAST);
//	lr_end_transaction("AS_Page_ClaimsList", LR_AUTO);
	
	lr_end_sub_transaction("RMS_AutoSource_T16_ClickClose2_ST13_Page_ClaimsList", LR_AUTO);
	
	web_revert_auto_header("Origin");

//	lr_start_transaction("AS_CiecaSettings_CiecaAclUrl");
//		web_url("CiecaAclUrl_4", 
//			"URL={ADXE_URL}/api/CiecaSettings/CiecaAclUrl", 
//			"Resource=0", 
//			"RecContentType=text/plain", 
//			"Referer={ADXE_URL}/Estimating/WorkList", 
//			"Snapshot=t108.inf", 
//			"Mode=HTML", 
//			LAST);
//	lr_end_transaction("AS_CiecaSettings_CiecaAclUrl", LR_AUTO);
//	
//	Cieca_import();

lr_end_transaction("RMS_AutoSource_T16_ClickClose2",LR_AUTO);
	return 0;
}
