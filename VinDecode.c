VinDecode()
{
	
lr_start_transaction("RMS_AutoSource_T07_VINDecode");

//	lr_start_sub_transaction("RMS_AutoSource_T07_VINDecode_ST00_Service_VidSvr_DecodeVehicle", "RMS_AutoSource_T07_VINDecode");

		web_add_auto_header("Origin", 
			"{ADXE_URL}");
		
		web_reg_save_param_ex("ParamName=VID_captured", "LB=\"VehicleID\":{\"Item\":[\"", "RB=\"",
			SEARCH_FILTERS,
			"Scope=Body",
			"IgnoreRedirections=No",
			LAST);

		web_reg_save_param_ex("ParamName=VIN_Status", "LB=\"VINStatus\":\"",	"RB=\"",
			SEARCH_FILTERS,
			"Scope=Body",
			"IgnoreRedirections=No",
			LAST);
		
		web_custom_request("VidSvr", 
			"URL={ADXE_URL}/api/Service/VidSvr", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Vehicle", 
			"Snapshot=t52.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"Request\":{\"@Method\":\"DecodeVehicle\",\"@type\":\"VINDecode\",\"DoorLabel\":\"D\",\"DriveLabel\":\"WD\",\"VIN\":\"1HGCR3F85FA01{VINRand1}{VINRand2}\"}}}", 
			LAST);

	lr_end_transaction("RMS_AutoSource_T07_VINDecode",LR_AUTO);

//	lr_end_sub_transaction("RMS_AutoSource_T07_VINDecode_ST00_Service_VidSvr_DecodeVehicle", LR_AUTO);
	
	lr_think_time(TT);

	
lr_start_transaction("RMS_AutoSource_T08_SelectVehicle");

	lr_start_sub_transaction("RMS_AutoSource_T08_SelectVehicle_ST00_Service_VidSvr_LookupVehicle", "RMS_AutoSource_T08_SelectVehicle");
		
			web_reg_save_param_ex("ParamName=EngineCode", "LB=\"Engine\":{\"Item\":[{\"@code\":\"", "RB=\"",
				SEARCH_FILTERS,
				"Scope=Body",
				"IgnoreRedirections=No",
				LAST);

			web_reg_save_param_regexp("ParamName=EngineNumber",
			    	"RegExp=\"Engine\":{\"Item\":\\\[{\"@code\":\"(.{1})\",\"@number\":\"(.{4})\",\"#text\":\"([a-z,A-Z,0-9,\., ,]+)\"},{",
	                 	"Group=2",
			       "Notfound=warning", 
			       LAST);		

			web_reg_save_param_regexp("ParamName=EngineText",
			    	"RegExp=\"Engine\":{\"Item\":\\\[{\"@code\":\"(.{1})\",\"@number\":\"(.{4})\",\"#text\":\"([a-z,A-Z,0-9,\., ,]+)\"},{",
	                 	"Group=3",
			       "Notfound=warning", 
			       LAST);	
		
			web_reg_save_param_ex("ParamName=Typical_Value", "LB=\"TypicalValue\":\"", "RB=\"",
				SEARCH_FILTERS,
				"Scope=Body",
				"IgnoreRedirections=No",
				LAST);
		
			web_reg_save_param_ex("ParamName=Veh_Desc1", "LB=\"VehicleDescription1\":\"", "RB=\"",
				SEARCH_FILTERS,
				"Scope=Body",
				"IgnoreRedirections=No",
				LAST);
			web_reg_save_param_ex("ParamName=Veh_Desc2", "LB=\"VehicleDescription2\":\"", "RB=\"",
				SEARCH_FILTERS,
				"Scope=Body",
				"IgnoreRedirections=No",
				LAST);
		
			web_custom_request("VidSvr_2", 
				"URL={ADXE_URL}/api/Service/VidSvr", 
				"Method=POST", 
				"Resource=0", 
				"RecContentType=text/plain", 
				"Referer={ADXE_URL}/Estimating/Autosource/Vehicle", 
				"Snapshot=t53.inf", 
				"Mode=HTML", 
				"EncType=application/json;charset=UTF-8", 
				"Body={\"Form\":{\"Request\":{\"@Method\":\"LookupVehicle\",\"@type\":\"undefined\",\"DoorLabel\":\"D\",\"DriveLabel\":\"WD\",\"VIN\":\"\",\"VehId\":\"{VID_captured}\"}}}", 
				LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T08_SelectVehicle_ST00_Service_VidSvr_LookupVehicle", LR_AUTO);
		
	
	lr_start_sub_transaction("RMS_AutoSource_T08_SelectVehicle_ST01_Service_VidSvr_SetOptions", "RMS_AutoSource_T08_SelectVehicle");
	
			web_reg_save_param_ex("ParamName=VID_captured1", "LB=\"Response\":{\"@VID\":\"",	"RB=\"",
				SEARCH_FILTERS,
				"Scope=Body",
				"IgnoreRedirections=No",
				LAST);
			web_custom_request("VidSvr_3", 
				"URL={ADXE_URL}/api/Service/VidSvr", 
				"Method=POST", 
				"Resource=0", 
				"RecContentType=text/plain", 
				"Referer={ADXE_URL}/Estimating/Autosource/Vehicle", 
				"Snapshot=t54.inf", 
				"Mode=HTML", 
				"EncType=application/json;charset=UTF-8", 
				"Body={\"Form\":{\"Request\":{\"@Method\":\"SetOptions\",\"@type\":\"undefined\",\"ExteriorOpt\":0,\"InteriorOpt\":-1,\"Item\":[\"AAI\",\"ABS\",\"AC \",\"ADM\",\"AHC\",\"ALR\",\"ALW\",\"AMP\",\"BST\",\"CC \",\"CD \",\"CHG\",\"CPS\",\"CTC\",\"DAB\",\"DEF\",\"DHM\",\"DSM\",\"DZA\",\"ES2\",\"EST\",\"FCA\",\"FLM\",\"FOG\",\"GDO\",\"HAB\",\"HAH\",\"HFS\",\"INW\",\"IVM\",\"KES\",\"LCD\",\"LDA\",\"LED\",\"LES\",\"LRL\",\"LSW\",\"LTH\",\"MP3\",\"OHC\",\"PB \",\"PDL\",\"PL \",\"PMO\",\"PW \",\"RBS\",\"RHA\",\"RTR\",\"RVC\",\"SAB\",\"SCS\",\"SVC\",\"SWC\",\"SXS\",\"TCH\",\"TCM\",\"TCS\",\"TNT\",\"TPM\",\"TTW\",\"WPC\",\"WST\",\"\"]," 
				"\"UOpt\":[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"CZ\",\"\",\"FC\",\"\",\"\",\"\",\"HX\",\"\",\"\",\"\",\"\",\"2I\",\"9R\",\"\",\"DY\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"MR\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"JP\"],\"" 
				"VID\":\"{VID_captured}\"}}}",
				LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T08_SelectVehicle_ST01_Service_VidSvr_SetOptions", LR_AUTO);
	
		/* This Value changes with the vehicle type*/
		lr_save_string("233353","VehicleNumber");
		
	lr_start_sub_transaction("RMS_AutoSource_T08_SelectVehicle_ST02_Service_VidSvr_GETODOMTYPICAL", "RMS_AutoSource_T08_SelectVehicle");
		
			web_custom_request("FaicSvr_2", 
				"URL={ADXE_URL}/api/Service/VidSvr", 
				"Method=POST", 
				"Resource=0", 
				"RecContentType=text/plain", 
				"Referer={ADXE_URL}/Estimating/Autosource/Vehicle", 
				"Snapshot=t55.inf", 
				"Mode=HTML", 
				"EncType=application/json;charset=UTF-8", 
				"Body={\"Form\":{\"Request\":{\"@Method\":\"GETODOMTYPICAL\",\"DAL_INPUT\":{\"VEHICLE_NBR\":\"{VehicleNumber}\",\"ZIP_5\":\"{ZipCode}\"}}}}", 
				LAST);
		
	lr_end_sub_transaction("RMS_AutoSource_T08_SelectVehicle_ST02_Service_VidSvr_GETODOMTYPICAL", LR_AUTO);
		

lr_end_transaction("RMS_AutoSource_T08_SelectVehicle",LR_AUTO);

lr_think_time(TT);
	
lr_start_transaction("RMS_AutoSource_T09_GetTransmissions");

	lr_start_sub_transaction("RMS_AutoSource_T09_GetTransmissions_ST00_Service_GetTransmissions", "RMS_AutoSource_T09_GetTransmissions");
		
			web_reg_save_param_ex("ParamName=TransCode", "LB=\"Item\":{\"@code\":\"",	"RB=\"",
				SEARCH_FILTERS,
				"Scope=Body",
				"IgnoreRedirections=No",
				LAST);
			web_reg_save_param_ex("ParamName=TransText", "LB=\"#text\":\"",	"RB=\"",
				SEARCH_FILTERS,
				"Scope=Body",
				"IgnoreRedirections=No",
				LAST);
			web_custom_request("VidSvr_4",
				"URL={ADXE_URL}/api/Service/VidSvr", 
				"Method=POST", 
				"Resource=0", 
				"RecContentType=text/plain", 
				"Referer={ADXE_URL}/Estimating/Autosource/Vehicle", 
				"Snapshot=", 
				"Mode=HTML", 
				"EncType=application/json;charset=UTF-8", 
				"Body={\"Form\":{\"Request\":{\"@Method\":\"GetTransmissions\",\"@type\":\"undefined\",\"Engine\":\"0\",\"Make\":5,\"Model\":0,\"Region\":\"Asian\",\"Style\":7,\"Year\":4}}}", 
				LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T09_GetTransmissions_ST00_Service_GetTransmissions", LR_AUTO);

	
	lr_start_sub_transaction("RMS_AutoSource_T09_GetTransmissions_ST01_Service_VidSvr_GetOptions", "RMS_AutoSource_T09_GetTransmissions");
	
			web_reg_save_param_ex("ParamName=VID_captured1", "LB=\"Response\":{\"@VID\":\"",	"RB=\"",
				SEARCH_FILTERS,
				"Scope=Body",
				"IgnoreRedirections=No",
				LAST);
		
			web_custom_request("VidSvr_4", 
				"URL={ADXE_URL}/api/Service/VidSvr", 
				"Method=POST", 
				"Resource=0", 
				"RecContentType=text/plain", 
				"Referer={ADXE_URL}/Estimating/Autosource/Vehicle", 
				"Snapshot=", 
				"Mode=HTML", 
				"EncType=application/json;charset=UTF-8", 
				"Body={\"Form\":{\"Request\":{\"@Method\":\"GetOptions\",\"@type\":\"undefined\",\"Engine\":\"0\",\"Make\":5,\"Model\":0,\"Region\":\"Asian\",\"Style\":7,\"Transmission\":\"0\",\"Year\":4}}}", 
				LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T09_GetTransmissions_ST01_Service_VidSvr_GetOptions", LR_AUTO);
	
	lr_start_sub_transaction("RMS_AutoSource_T09_GetTransmissions_ST02_Service_VidSvr_SetOptions", "RMS_AutoSource_T09_GetTransmissions");
	
			web_reg_save_param_ex("ParamName=VID_captured1", "LB=\"Response\":{\"@VID\":\"",	"RB=\"",
				SEARCH_FILTERS,
				"Scope=Body",
				"IgnoreRedirections=No",
				LAST);
			web_custom_request("VidSvr_3", 
				"URL={ADXE_URL}/api/Service/VidSvr", 
				"Method=POST", 
				"Resource=0", 
				"RecContentType=text/plain", 
				"Referer={ADXE_URL}/Estimating/Autosource/Vehicle", 
				"Snapshot=t54.inf", 
				"Mode=HTML", 
				"EncType=application/json;charset=UTF-8", 
				"Body={\"Form\":{\"Request\":{\"@Method\":\"SetOptions\",\"@type\":\"undefined\",\"ExteriorOpt\":0,\"InteriorOpt\":-1,\"Item\":[\"AAI\",\"ABS\",\"AC \",\"ADM\",\"AHC\",\"ALR\",\"ALW\",\"AMP\",\"BST\",\"CC \",\"CD \",\"CHG\",\"CPS\",\"CTC\",\"DAB\",\"DEF\",\"DHM\",\"DSM\",\"DZA\",\"ES2\",\"EST\",\"FCA\",\"FLM\",\"FOG\",\"GDO\",\"HAB\",\"HAH\",\"HFS\",\"INW\",\"IVM\",\"KES\",\"LCD\",\"LDA\",\"LED\",\"LES\",\"LRL\",\"LSW\",\"LTH\",\"MP3\",\"OHC\",\"PB \",\"PDL\",\"PL \",\"PMO\",\"PW \",\"RBS\",\"RHA\",\"RTR\",\"RVC\",\"SAB\",\"SCS\",\"SVC\",\"SWC\",\"SXS\",\"TCH\",\"TCM\",\"TCS\",\"TNT\",\"TPM\",\"TTW\",\"WPC\",\"WST\",\"\"]," 
				"\"UOpt\":[\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"CZ\",\"\",\"FC\",\"\",\"\",\"\",\"HX\",\"\",\"\",\"\",\"\",\"2I\",\"9R\",\"\",\"DY\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"MR\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"JP\"],\"" 
				"VID\":\"{VID_captured1}\"}}}",
				LAST);
		
	lr_end_sub_transaction("RMS_AutoSource_T09_GetTransmissions_ST02_Service_VidSvr_SetOptions", LR_AUTO);
		
lr_end_transaction("RMS_AutoSource_T09_GetTransmissions", LR_AUTO);
		
		


//	165,600
	lr_save_string("57,584","TypicalMileage");
	
	lr_save_string("AAI|Auxiliary Audio Input|S|0|undefined|undefined|^ABS|Anti-Lock Brakes|S|1|undefined|undefined|^AC |Air Conditioning|S|2|undefined|undefined|^ADM|Automatic Dimming Mirror|S|3|undefined|undefined|^AHC|Auto Headlamp Control|S|4|undefined|undefined|^ALR|Alarm System|S|5|undefined|undefined|^ALW|Aluminum/Alloy Wheels|S|6|undefined|undefined|^AMP|Amplifier|S|7|undefined|undefined|^BST|Bucket Seats|S|8|undefined|undefined|^CC |Cruise Control|S|9|undefined|undefined|^CD |AM/FM CD Player|S|10|undefined|undefined|^CHG|Chrome Grille|S|11|undefined|undefined|^CPS|Compact Spare Tire|S|12|undefined|undefined|^CTC|Center Console|S|13|undefined|undefined|^DAB|Dual Airbags|S|14|undefined|undefined|^DEF|Rear Window Defroster|S|15|undefined|undefined|^DHM|Heated Power Mirrors|S|16|undefined|undefined|^DSM|Driver Seat Memory|S|17|undefined|undefined|^DZA|Dual Zone Auto A/C|S|18|undefined|undefined|^ES2|Dual Power Seats|S|19|undefined|undefined|^EST|Electric Steering|S|20|undefined|undefined|^FCA|Fwd. Collision Alert|S|21|undefined|undefined|CZ^FLM|Floor Mats|S|22|undefined|undefined|^FOG|Fog Lights|S|23|undefined|undefined|FC^GDO|Garage Door Opener|S|24|undefined|undefined|^HAB|Head Airbags|S|25|undefined|undefined|^HAH|Halogen Headlights|S|26|undefined|undefined|^HFS|Heated Front Seats|S|27|undefined|undefined|HX^INW|Intermittent Wipers|S|28|undefined|undefined|^IVM|Illuminated Visor Mirror|S|29|undefined|undefined|^KES|Keyless Entry System|S|30|undefined|undefined|^LCD|1st Row LCD Monitor(s)|S|31|undefined|undefined|^LDA|Lane Departure Alert|S|32|undefined|undefined|2I^LED|LED Brakelights|S|33|undefined|undefined|9R^LES|Lighted Entry System|S|34|undefined|undefined|^LRL|LED Daytime Running Lts|S|35|undefined|undefined|DY^LSW|Leather Steering Wheel|S|36|undefined|undefined|^LTH|Leather Seats|S|37|undefined|undefined|^MP3|MP3 Decoder|S|38|undefined|undefined|^OHC|Overhead Console|S|40|undefined|undefined|^PB |Power Brakes|S|41|undefined|undefined|^PDL|Pwr Driver Lumbar Supp|S|42|undefined|undefined|^PL |Power Door Locks|S|43|undefined|undefined|^PMO|Power Moonroof|S|44|undefined|undefined|MR^PW |Power Windows|S|45|undefined|undefined|^RBS|Rear Bench Seat|S|46|undefined|undefined|^RHA|2nd Row Head Airbags|S|47|undefined|undefined|^RTR|Rem Trunk-L/Gate Release|S|49|undefined|undefined|^RVC|Rear View Camera|S|50|undefined|undefined|^SAB|Side Airbags|S|51|undefined|undefined|^SCS|Stability Cntrl Suspensn|S|52|undefined|undefined|^SVC|Side View Camera(s)|S|53|undefined|undefined|^SWC|Strg Wheel Radio Control|S|54|undefined|undefined|^SXS|SiriusXM Satellite Radio|S|55|undefined|undefined|^TCH|Tachometer|S|56|undefined|undefined|^TCM|Trip Computer|S|57|undefined|undefined|^TCS|Traction Control System|S|58|undefined|undefined|^TNT|Tinted Glass|S|59|undefined|undefined|^TPM|Tire Pressure Monitor|S|60|undefined|undefined|^TTW|Tilt & Telescopic Steer|S|61|undefined|undefined|^WPC|Wireless Phone Connect|S|62|undefined|undefined|^WST|Wireless Audio Streaming|S|63|undefined|undefined|^|Japan Built Vehicle|S|67|undefined|undefined|JP","HID_Options");
	return 0;
}
