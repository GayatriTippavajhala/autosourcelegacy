AS_ProductSelection()
{

lr_start_transaction("RMS_AutoSource_T03_SelectNewASProduct");

	lr_start_sub_transaction("RMS_AutoSource_T03_SelectNewASProduct_ST00_Page_ProfileSelect", "RMS_AutoSource_T03_SelectNewASProduct");
	
		web_reg_save_param_ex("ParamName=ProfileID", "LB={\"@value\":\"", "RB=\",\"@worksharingflg\"", "Ordinal=ALL",
			SEARCH_FILTERS,
			"Scope=BODY",
			LAST);
	
		web_url("ProfileSelect", 
			"URL={ADXE_URL}/api/Page/ProfileSelect", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/WorkList", 
			"Snapshot=t37.inf", 
			"Mode=HTML", 
			LAST);

	lr_end_sub_transaction("RMS_AutoSource_T03_SelectNewASProduct_ST00_Page_ProfileSelect", LR_AUTO);
	
	lr_save_string(lr_eval_string("{ProfileID_2}"),"Profile_Id");

	lr_start_sub_transaction("RMS_AutoSource_T03_SelectNewASProduct_ST01_Service_NewClaim", "RMS_AutoSource_T03_SelectNewASProduct");
	
		web_custom_request("NewClaim", 
			"URL={ADXE_URL}/api/Service/NewClaim", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/html", 
			"Referer={ADXE_URL}/Estimating/WorkList", 
			"Snapshot=t38.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"ClaimGUID\":\"\",\"ClaimType\":\"\",\"ItemType\":\"\",\"LinkedItem\":\"\",\"OpenContext\":\"1\",\"PDEstFlag\":\"0\",\"Permission\":\"1\",\"ProfileId\":[\"{Profile_Id}\"],\"ProfileType\":\"\"}}", 
			LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T03_SelectNewASProduct_ST01_Service_NewClaim", LR_AUTO);

	web_add_auto_header("Sec-Fetch-Site", "same-origin");

	
	lr_start_sub_transaction("RMS_AutoSource_T03_SelectNewASProduct_ST02_Page_ASProductSel", "RMS_AutoSource_T03_SelectNewASProduct");
	
		web_reg_save_param_ex("ParamName=ClaimGUID", "LB=\"caption\":\"{GUID}\",\"value\":\"", "RB=\"",
			SEARCH_FILTERS,
			"Scope=BODY",
			LAST);
		
		web_url("ASProductSel", 
			"URL={ADXE_URL}/api/Page/ASProductSel", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/NewProductSelection", 
			"Snapshot=t43.inf", 
			"Mode=HTML", 
			LAST);
	
	lr_end_sub_transaction("RMS_AutoSource_T03_SelectNewASProduct_ST02_Page_ASProductSel", LR_AUTO);


lr_end_transaction("RMS_AutoSource_T03_SelectNewASProduct",LR_AUTO);

	lr_think_time(TT);
	
lr_start_transaction("RMS_AutoSource_T04_EnterZipCode");

	web_add_auto_header("Origin", "{ADXE_URL}");

		web_reg_save_param_ex("ParamName=TaxCity", "LB=\"TAX_CITY\":\"",	"RB=\"",
				SEARCH_FILTERS,
				"Scope=Body",
				"IgnoreRedirections=No",
				LAST);
	
		web_custom_request("FaicSvr", 
			"URL={ADXE_URL}/api/Service/FaicSvr", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/NewProductSelection", 
			"Snapshot=t44.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"Request\":{\"@Method\":\"GETSTATECITYINFO\",\"DAL_INPUT\":{\"CITY_ZIP_5\":\"{ZipCode}\"}}}}", 
			LAST);

lr_end_transaction("RMS_AutoSource_T04_EnterZipCode",LR_AUTO);

	lr_think_time(TT);
	
lr_start_transaction("RMS_AutoSource_T05_SelectProduct");

	lr_start_sub_transaction("RMS_AutoSource_T05_SelectProduct_ST00_Service_ClaimUpdate", "RMS_AutoSource_T05_SelectProduct");
	
		web_custom_request("ClaimUpdate", 
			"URL={ADXE_URL}/api/Service/ClaimUpdate", 
			"Method=POST", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/NewProductSelection", 
			"Snapshot=t45.inf", 
			"Mode=HTML", 
			"EncType=application/json;charset=UTF-8", 
			"Body={\"Form\":{\"GUID\":[{\"name\":\"GUID\",\"caption\":\"{GUID}\",\"value\":\"{ClaimGUID}\"},{\"name\":\"GUID\",\"caption\":\"{GUID}\",\"value\":\"{ClaimGUID}\"}],\"ICBCmode\":{\"fieldtype\":\"hiddendata\",\"name\":\"ICBCmode\",\"caption\":\"{ICBCmode}\",\"value\":[\"N\",null]},\"ValProdUsed\":{\"fieldtype\":\"hiddendata\",\"name\":\"ValProdUsed\",\"caption\":\"{ValProdUsed}\",\"value\":\"YES\"},\"CurrentProdClass\":{\"fieldtype\":\"hiddendata\","
			"\"name\":\"CurrentProdClass\",\"caption\":\"{CurrentProdClass}\",\"value\":null},\"ASNewProdMode\":{\"fieldtype\":\"hiddendata\",\"name\":\"ASNewProdMode\",\"caption\":\"{ASNewProdMode}\",\"value\":null},\"ProductId\":{\"fieldtype\":\"hiddendata\",\"name\":\"ProductId\",\"caption\":\"{ProductId}\",\"value\":\"54\"},\"CreatedForProfileId\":{\"name\":\"CreatedForProfileId\",\"caption\":\"{CreatedForProfileId}\",\"value\":\"{Profile_Id}\"},\"EstimateStatus\":[{\"name\":\"EstimateStatus\",\"caption\":"
			"\"Status:\",\"combovalue\":[{\"@value\":\"0\",\"#text\":\"Assigned\"},{\"@value\":\"1\",\"#text\":\"In Progress\"},{\"@value\":\"2\",\"#text\":\"Completed Estimate Only\"},{\"@value\":\"3\",\"#text\":\"Completed Estimate, Notes, Forms\"},{\"@value\":\"4\",\"#text\":\"Template\"},{\"@value\":\"5\",\"#text\":\"Assgn Dup (Assignment Duplicate)\"},{\"@value\":\"6\",\"#text\":\"Assgn Comp (Assignment Completed)\"},{\"@value\":\"7\",\"#text\":\"Ready\"},{\"@value\":\"8\",\"#text\":\"Est Dup (Estimate "
			"Duplicate)\"},{\"@value\":\"9\",\"#text\":\"Report\"},{\"@value\":\"10\",\"#text\":\"Assgn Supp\"},{\"@value\":\"11\",\"#text\":\"Cancelled\"},{\"@value\":\"12\",\"#text\":\"Pending Approval\"},{\"@value\":\"13\",\"#text\":\"Not Approved\"},{\"@value\":\"14\",\"#text\":\"Approved\"},{\"@value\":\"15\",\"#text\":\"Rev Assigned\"},{\"@value\":\"16\",\"#text\":\"Update In Progress\"},{\"@value\":\"17\",\"#text\":\"Agreement Pending\"},{\"@value\":\"18\",\"#text\":\"Update Completed\"},{\"@value\":\""
			"19\",\"#text\":\"Completed with Revisions\"},{\"@value\":\"20\",\"#text\":\"Assigned Reinspection\"},{\"@value\":\"21\",\"#text\":\"In Progress Reinspection\"},{\"@value\":\"22\",\"#text\":\"Completed as Reference\"},{\"@value\":\"23\",\"#text\":\"Imported\"},{\"@value\":\"24\",\"#text\":\"Completed as Record\"},{\"@value\":\"25\",\"#text\":\"Sent for Reinspection\"},{\"@value\":\"26\",\"#text\":\"Retrieved\"},{\"@value\":\"27\",\"#text\":\"Return Review\"},{\"@value\":\"28\",\"#text\":\"Supp "
			"Notice\"}],\"value\":\"0\"},{\"fieldset\":\"Estimate\",\"fieldtype\":\"hiddendata\",\"name\":\"EstimateStatus\",\"caption\":\"Status:\",\"combovalue\":[{\"@value\":\"0\",\"#text\":\"Assigned\"},{\"@value\":\"1\",\"#text\":\"In Progress\"},{\"@value\":\"2\",\"#text\":\"Completed Estimate Only\"},{\"@value\":\"3\",\"#text\":\"Completed Estimate, Notes, Forms\"},{\"@value\":\"4\",\"#text\":\"Template\"},{\"@value\":\"5\",\"#text\":\"Assgn Dup (Assignment Duplicate)\"},{\"@value\":\"6\",\"#text\":\""
			"Assgn Comp (Assignment Completed)\"},{\"@value\":\"7\",\"#text\":\"Ready\"},{\"@value\":\"8\",\"#text\":\"Est Dup (Estimate Duplicate)\"},{\"@value\":\"9\",\"#text\":\"Report\"},{\"@value\":\"10\",\"#text\":\"Assgn Supp\"},{\"@value\":\"11\",\"#text\":\"Cancelled\"},{\"@value\":\"12\",\"#text\":\"Pending Approval\"},{\"@value\":\"13\",\"#text\":\"Not Approved\"},{\"@value\":\"14\",\"#text\":\"Approved\"},{\"@value\":\"15\",\"#text\":\"Rev Assigned\"},{\"@value\":\"16\",\"#text\":\"Update In "
			"Progress\"},{\"@value\":\"17\",\"#text\":\"Agreement Pending\"},{\"@value\":\"18\",\"#text\":\"Update Completed\"},{\"@value\":\"19\",\"#text\":\"Completed with Revisions\"},{\"@value\":\"20\",\"#text\":\"Assigned Reinspection\"},{\"@value\":\"21\",\"#text\":\"In Progress Reinspection\"},{\"@value\":\"22\",\"#text\":\"Completed as Reference\"},{\"@value\":\"23\",\"#text\":\"Imported\"},{\"@value\":\"24\",\"#text\":\"Completed as Record\"},{\"@value\":\"25\",\"#text\":\"Sent for Reinspection\"},{\""
			"@value\":\"26\",\"#text\":\"Retrieved\"},{\"@value\":\"27\",\"#text\":\"Return Review\"},{\"@value\":\"28\",\"#text\":\"Supp Notice\"}],\"value\":\"0\"}],\"EstimateActiveFlag\":{\"fieldtype\":\"hiddendata\",\"name\":\"EstimateActiveFlag\",\"caption\":\"{EstimateActiveFlag}\",\"value\":\"1\"},\"AutoSourceActiveFlag\":{\"fieldtype\":\"hiddendata\",\"name\":\"AutoSourceActiveFlag\",\"caption\":\"{AutoSourceActiveFlag}\",\"value\":\"1\"},\"MAZipCode\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\""
			",\"datatype\":\"aspostal\",\"style\":\"width:100%\",\"maxlength\":\"9\",\"onchange\":\"DecodeZip();\",\"mandatory\":\"*\",\"name\":\"MAZipCode\",\"caption\":\"Zip Code:\",\"value\":\"{ZipCode}\"},\"MAStateName\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"string\",\"style\":\"width:100%\",\"name\":\"MAStateName\",\"caption\":\"State:\",\"value\":\"\"},\"MAState\":{\"fieldtype\":\"hiddendata\",\"name\":\"MAState\",\"caption\":\"{MAState}\",\"value\":\"Texas\"},\"ProdNbr\":{\""
			"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"size\":\"6\",\"spaces\":\"yes\",\"onchange\":\"OnProductChange()\",\"style\":\"width:100%\",\"mandatory\":\"*\",\"name\":\"ProdNbr\",\"caption\":\"Product:\",\"value\":\"54\"},\"MAHValidProductString\":{\"fieldtype\":\"hiddendata\",\"name\":\"MAHValidProductString\",\"caption\":\"{MAHValidProductString}\",\"value\":\"\"},\"MAHStateMaxWrapDays\":{\"fieldtype\":\"hiddendata\",\"name\":\"MAHStateMaxWrapDays\",\"caption\":\"{MAHStateMaxWrapDays}\","
			"\"value\":null},\"MAHCustProducts\":{\"fieldtype\":\"itemlist\",\"name\":\"MAHCustProducts\",\"caption\":\"{MAHCustProducts}\",\"value\":null,\"combovalue\":[{\"@value\":\"38\",\"@abbrev\":\"M\",\"#text\":\"Basic Market Search\"},{\"@value\":\"465\",\"@abbrev\":\"V\",\"#text\":\"Valuation for S.G.I.\"},{\"@value\":\"54\",\"@abbrev\":\"V\",\"#text\":\"Test Prod For TLDG\"}]},\"MACity\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"dropdown\",\"style\":\"width:100%\",\"onchange\":\"OnCityChange()\",\""
			"spaces\":\"yes\",\"mandatory\":\"*\",\"name\":\"MACity\",\"caption\":\"City:\",\"value\":\"{TaxCity}\"},\"MAHCountryCode\":{\"fieldtype\":\"hiddendata\",\"name\":\"MAHCountryCode\",\"caption\":\"{MAHCountryCode}\",\"value\":\"Us\"},\"MAHOwnerPhone\":{\"fieldtype\":\"hiddendata\",\"datatype\":\"phone\",\"name\":\"MAHOwnerPhone\",\"caption\":\"{MAHOwnerPhone}\",\"value\":\"\"},\"ButtonGO\":{\"fieldset\":\"Estimate\",\"fieldtype\":\"button\",\"onclick\":\"ValidateB4Submit()\",\"disabled\":\"true\",\""
			"name\":\"ButtonGO\",\"caption\":\"Submit Product\",\"value\":null},\"RequestIsNew\":{\"fieldtype\":\"hiddendata\",\"name\":\"RequestIsNew\",\"caption\":\"{RequestIsNew}\",\"value\":\"True\"},\"DecodeFailed\":{\"fieldtype\":\"hiddendata\",\"name\":\"DecodeFailed\",\"caption\":\"{DecodeFailed}\",\"value\":\"True\"},\"ZipIsBlank\":{\"fieldtype\":\"hiddendata\",\"name\":\"ZipIsBlank\",\"caption\":\"{ZipIsBlank}\",\"value\":\"True\"},\"StateDefaultProductList\":{\"fieldtype\":\"itemlist\",\"name\":\""
			"StateDefaultProductList\",\"caption\":\"{StateDefaultProductList}\",\"value\":null,\"combovalue\":[{\"@value\":\"54\",\"#text\":\"Basic Vehicle Valuation\"},{\"@value\":\"153\",\"#text\":\"Valuation-New Graph/Phone #\"},{\"@value\":\"282\",\"#text\":\"Valuation\"},{\"@value\":\"364\",\"#text\":\"Connecticut Product(pa) Wnicb\"},{\"@value\":\"633\",\"#text\":\"Basic WA Product\"},{\"@value\":\"638\",\"#text\":\"PA Book Avg Val (AS and NADA)\"}]},\"DefaultProductUsed\":{\"fieldtype\":\"hiddendata\","
			"\"name\":\"DefaultProductUsed\",\"caption\":\"{DefaultProductUsed}\",\"value\":null},\"ProdDescription\":{\"fieldtype\":\"hiddendata\",\"name\":\"ProdDescription\",\"caption\":\"Product Desc:\",\"value\":\"Test Prod For TLDG\"},\"MAStateCode\":{\"fieldtype\":\"hiddendata\",\"name\":\"MAStateCode\",\"caption\":\"{MAStateCode}\",\"value\":\"TX\"},\"TLReinspectionMode\":{\"fieldtype\":\"hiddendata\",\"name\":\"TLReinspectionMode\",\"caption\":\"{TLReinspectionMode}\",\"value\":\"N\"},\"ReqReady\":{\""
			"#text\":\"1\",\"value\":null},\"ClaimNo\":{\"@custom\":\"1\",\"fieldset\":\"Estimate\",\"fieldtype\":\"input\",\"datatype\":\"claimnumber\",\"size\":\"20\",\"CustId\":\"APL\",\"mask\":\"\",\"maskEmpty\":\"\",\"maskSize\":\"25\",\"name\":\"ClaimNo\",\"caption\":\"Claim # :\",\"value\":null},\"Label_VehDescription\":{\"name\":\"Label_VehDescription\",\"caption\":\"Desc:\",\"value\":null},\"VehicleDescription\":{\"fieldset\":\"Estimate\",\"datatype\":\"string\",\"name\":\"VehicleDescription\",\""
			"caption\":\"Vehicle Description:\",\"value\":null},\"ValuationStatus\":{\"fieldtype\":\"statictrans\",\"name\":\"ValuationStatus\",\"caption\":\"Status:\",\"combovalue\":[{\"@value\":\"0\",\"#text\":\"In Progress\"},{\"@value\":\"1\",\"#text\":\"In Progress\"},{\"@value\":\"3\",\"#text\":\"Completed\"},{\"@value\":\"7\",\"#text\":\"Ready\"},{\"@value\":\"WIP\",\"#text\":\"Work In Progress\"},{\"@value\":\"COM\",\"#text\":\"Completed\"},{\"@value\":\"CAN\",\"#text\":\"Cancelled\"},{\"@value\":\""
			"RCV\",\"#text\":\"Received\"},{\"@value\":\"FOR\",\"#text\":\"Forwarded\"}],\"value\":\"1\"},\"Check_CommitValuation\":{\"fieldtype\":\"checkbox_single\",\"OnClick\":\"onSelectReady()\",\"name\":\"Check_CommitValuation\",\"caption\":\"Request Ready\",\"value\":null},\"ShoplinkLiteVal\":{\"fieldtype\":\"hiddendata\",\"name\":\"ShoplinkLiteVal\",\"caption\":\"{ShoplinkLiteVal}\",\"value\":null},\"BaseXMLType\":\"Val\",\"SaveData\":\"1\",\"BaseXML\":\"ASProductSel.xml\"}}", 
			LAST);
	

	lr_end_sub_transaction("RMS_AutoSource_T05_SelectProduct_ST00_Service_ClaimUpdate", LR_AUTO);
		
	web_revert_auto_header("Origin");
	
	lr_start_sub_transaction("RMS_AutoSource_T05_SelectProduct_ST01_Page_Admin", "RMS_AutoSource_T05_SelectProduct");
	
		web_url("Admin", 
			"URL={ADXE_URL}/api/Page/Admin", 
			"Resource=0", 
			"RecContentType=text/plain", 
			"Referer={ADXE_URL}/Estimating/Autosource/Admin", 
			"Snapshot=t46.inf", 
			"Mode=HTML", 
			LAST);

	lr_end_sub_transaction("RMS_AutoSource_T05_SelectProduct_ST01_Page_Admin", LR_AUTO);

	web_add_auto_header("Origin", "{ADXE_URL}");
lr_end_transaction("RMS_AutoSource_T05_SelectProduct",LR_AUTO);
Lock();
	
UnLock();
	


	return 0;
}
